-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2018 at 01:54 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mediacomm_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `adscategory`
--

CREATE TABLE `adscategory` (
  `CatID` int(4) NOT NULL COMMENT 'Category ID Unique',
  `CatType` varchar(70) NOT NULL COMMENT 'Category Type For commercial ads',
  `Description` text COMMENT 'Category Description',
  `cat_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adscategory`
--

INSERT INTO `adscategory` (`CatID`, `CatType`, `Description`, `cat_image`) VALUES
(1, 'REALISM', 'Realism is the representation of whatever is possible in real life or in a non fictional', 'realism111.png'),
(2, 'ANTI REALISM ', 'Anti-realism is the representation of anything unrealistic, the use of fictional/unreal situations or ideas', 'antiRealism1.png'),
(3, 'ANIMATION ', 'Animation is the process creating frames one by one then displaying them quickly in sequence creating the illusion of movement. There are different styles of animation for example computer generated images (CGI which can be 3D or 2D), stop frame animation or clay-mation', 'solo-animation1.png'),
(4, 'DOCUMENTORTY ', 'Documentary style adverts tend to use real locations and sometimes real staff in order to establish trust and gain credibility with the audience, they also tend to give a lot of facts about how good the food is in order to gain more trust', 'Documentary_logo.png'),
(5, 'TALKING HEADS', 'Talking heads adverts tend to have people talking about the product/company and give their positive opinions. The positive aspects of this is that people believe they are receiving genuine advice from people who have used the product/company', 'talkingHead1.png'),
(6, 'STAND ALONE', 'A stand alone advertisement is a form of advert that is a one off, there will not be another advert by the same company that is related in terms of the story/narrative', 'stand-alone.png'),
(7, 'SERIES ', 'SERIES Some advertising campiness use a series of adverts to promote a single brand, product or service. Sometimes the adverts differ slightly in the content but it can also result in a progressive narrative over a long period of time and over a number of singular commercials', 'SeriesLogo1.png'),
(8, 'BANDWAGON TYPE', 'In this type of advertising, Creative producer show that everybody is buying that specific product so that u should buy it. For example advertising of Pepsi in which they show everybody is buying Pepsi why u doesn’t?', 'bandwagon1.png'),
(9, 'PUBLIC SERVICE Announcement', 'Commercial put on by the government for public safety or information, also known as public service messages. For examples Don’t smoke and buckle up before driving, obey the speed limits.', 'publicservice11.png'),
(10, 'TESTIMONAL', 'One of the most common type, the testimonial lecture testimony form an expert in a certain field. Expert gives suggestion to use that specific product. “We recommend you to use that”', 'testimonial2.png');

-- --------------------------------------------------------

--
-- Table structure for table `adsmasterinfo`
--

CREATE TABLE `adsmasterinfo` (
  `AdsID` int(4) NOT NULL COMMENT 'Ads ID Unique',
  `AdsName` varchar(70) NOT NULL COMMENT 'Name of Ads',
  `FileName` varchar(400) DEFAULT NULL,
  `CatID` int(4) NOT NULL DEFAULT '1' COMMENT 'Category of Advertiement',
  `AdDuration` decimal(15,3) DEFAULT NULL COMMENT 'Ad Duration Time',
  `AdSlot` decimal(15,3) DEFAULT NULL COMMENT 'Ad Slot',
  `ProductID` int(4) DEFAULT NULL COMMENT 'Product ID',
  `AgencyID` int(4) DEFAULT NULL COMMENT 'Ad Booking/Refer Agency ID',
  `ArtistID` int(4) DEFAULT NULL COMMENT 'Prime Artist in Ad',
  `AdRating` varchar(20) DEFAULT NULL COMMENT 'Rating of AD',
  `AdBookingDate` date NOT NULL COMMENT 'Ad Booking Date',
  `OrderNo` varchar(20) DEFAULT NULL COMMENT 'IF any Order No',
  `Cost` decimal(15,3) DEFAULT NULL COMMENT 'Cost of Ad',
  `FilePath` varchar(200) NOT NULL,
  `brandID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adsmasterinfo`
--

INSERT INTO `adsmasterinfo` (`AdsID`, `AdsName`, `FileName`, `CatID`, `AdDuration`, `AdSlot`, `ProductID`, `AgencyID`, `ArtistID`, `AdRating`, `AdBookingDate`, `OrderNo`, `Cost`, `FilePath`, `brandID`) VALUES
(113, '7up (1).mp4', '7up (1).mp4', 1, '17564734.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (1).mp4', 0),
(114, '7up (2).mp4', '7up (2).mp4', 1, '202548.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (2).mp4', 0),
(115, '7up (3).mp4', '7up (3).mp4', 1, '158012.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (3).mp4', 0),
(116, '7up (4).mp4', '7up (4).mp4', 1, '202061.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (4).mp4', 0),
(117, '7up (5).mp4', '7up (5).mp4', 1, '126108.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (5).mp4', 0),
(118, '7up (6).mp4', '7up (6).mp4', 1, '19974269.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (6).mp4', 0),
(119, '7 Up Desert Tvc-02 30sec.mov', '7 Up Desert Tvc-02 30sec.mov', 1, '981.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7 Up Desert Tvc-02 30sec.mov', 0),
(120, '7 Up tvc-11 (2007) redo.mov', '7 Up tvc-11 (2007) redo.mov', 1, '899.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7 Up tvc-11 (2007) redo.mov', 0),
(121, '7 Up tvc-11 (2007).mov', '7 Up tvc-11 (2007).mov', 1, '914.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7 Up tvc-11 (2007).mov', 0),
(122, '7up tape-01 (Desert tvc-03) 35sec.mp4', '7up tape-01 (Desert tvc-03) 35sec.mp4', 1, '3600000.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-03) 35sec.mp4', 0),
(123, '7up tape-01 (Desert tvc-04) 05sec.mp4', '7up tape-01 (Desert tvc-04) 05sec.mp4', 1, '1317120.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-04) 05sec.mp4', 0),
(124, '7up tape-01 (Desert tvc-29) 28sec.mp4', '7up tape-01 (Desert tvc-29) 28sec.mp4', 1, '3686400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-29) 28sec.mp4', 0),
(125, '7up tape-01 (Desert tvc-30) 28sec.mp4', '7up tape-01 (Desert tvc-30) 28sec.mp4', 1, '3538560.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-30) 28sec.mp4', 0),
(126, '7 Up Desert Tvc-02 30sec.mov', '7 Up Desert Tvc-02 30sec.mov', 1, '981.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7 Up Desert Tvc-02 30sec.mov', 0),
(127, '7 Up tvc-11 (2007) redo.mov', '7 Up tvc-11 (2007) redo.mov', 1, '899.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7 Up tvc-11 (2007) redo.mov', 0),
(128, '7 Up tvc-11 (2007).mov', '7 Up tvc-11 (2007).mov', 1, '914.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7 Up tvc-11 (2007).mov', 0),
(129, '7up tape-01 (Desert tvc-03) 35sec.mp4', '7up tape-01 (Desert tvc-03) 35sec.mp4', 1, '3600000.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-03) 35sec.mp4', 0),
(130, '7up tape-01 (Desert tvc-04) 05sec.mp4', '7up tape-01 (Desert tvc-04) 05sec.mp4', 1, '1317120.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-04) 05sec.mp4', 0),
(131, '7up tape-01 (Desert tvc-29) 28sec.mp4', '7up tape-01 (Desert tvc-29) 28sec.mp4', 1, '3686400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-29) 28sec.mp4', 0),
(132, '7up tape-01 (Desert tvc-30) 28sec.mp4', '7up tape-01 (Desert tvc-30) 28sec.mp4', 1, '3538560.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-30) 28sec.mp4', 0),
(133, '7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', '7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', 0),
(134, '7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', '7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', 0),
(135, '7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', '7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', 0),
(136, '7up tape-01 (Fido tvc-07) 38sec.mp4', '7up tape-01 (Fido tvc-07) 38sec.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec.mp4', 0),
(137, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', 0),
(138, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', 0),
(139, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', 0),
(140, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', 0),
(141, '7up tape-01 (Fido tvc-19) 45sec.mp4', '7up tape-01 (Fido tvc-19) 45sec.mp4', 1, '4241280.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-19) 45sec.mp4', 0),
(142, '7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', '7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', 1, '2035200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', 0),
(143, '7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', '7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', 1, '2035200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', 0),
(144, '7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', '7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', 1, '1036800.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', 0),
(145, '7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', '7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', 1, '1036800.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', 0),
(146, '7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', '7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', 1, '1036800.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', 0),
(147, '7up tape-01 (Fido tvc-21) 10sec.mp4', '7up tape-01 (Fido tvc-21) 10sec.mp4', 1, '1036800.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec.mp4', 0),
(148, '7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', '7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', 1, '493440.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', 0),
(149, '7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', '7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', 1, '493440.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', 0),
(150, 'Abbott.mp4', 'Abbott.mp4', 1, '1545600.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Abbott.mp4', 0),
(151, 'AIDS.mp4', 'AIDS.mp4', 1, '1545600.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/AIDS.mp4', 0),
(152, 'Ansar-Burney-Trust.mp4', 'Ansar-Burney-Trust.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Ansar-Burney-Trust.mp4', 0),
(153, 'Audi-Cars.mp4', 'Audi-Cars.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Audi-Cars.mp4', 0),
(154, 'B.P.mp4', 'B.P.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/B.P.mp4', 0),
(155, 'BAIT-UL-SUKOON.mp4', 'BAIT-UL-SUKOON.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/BAIT-UL-SUKOON.mp4', 0),
(156, 'BARBIE.mp4', 'BARBIE.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/BARBIE.mp4', 0),
(157, 'Barclays.mp4', 'Barclays.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Barclays.mp4', 0),
(158, 'BATA.mp4', 'BATA.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/BATA.mp4', 0),
(159, 'Bonanza.mp4', 'Bonanza.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Bonanza.mp4', 0),
(160, 'BRAUN.mp4', 'BRAUN.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/BRAUN.mp4', 0),
(161, 'Butterfly.mp4', 'Butterfly.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Butterfly.mp4', 0),
(162, 'Capstan-Cigarette.mp4', 'Capstan-Cigarette.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Capstan-Cigarette.mp4', 0),
(163, 'CASTROL.mp4', 'CASTROL.mp4', 1, '1545600.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/CASTROL.mp4', 0),
(164, 'Cheetos.mp4', 'Cheetos.mp4', 1, '1411200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Cheetos.mp4', 0),
(165, 'CITIBANK.mp4', 'CITIBANK.mp4', 1, '1411200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/CITIBANK.mp4', 0),
(166, 'Citizens-Foundation.mp4', 'Citizens-Foundation.mp4', 1, '1176960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Citizens-Foundation.mp4', 0),
(167, 'Coca-Cola.mp4', 'Coca-Cola.mp4', 1, '1411200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Coca-Cola.mp4', 0),
(168, 'Colgate.mp4', 'Colgate.mp4', 1, '1176960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Colgate.mp4', 0),
(169, 'Cricket.mp4', 'Cricket.mp4', 1, '1176960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Cricket.mp4', 0),
(170, 'Dalda-Foods.mp4', 'Dalda-Foods.mp4', 1, '1176960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Dalda-Foods.mp4', 0),
(171, 'Depilex-Smileagain-Foundation.mp4', 'Depilex-Smileagain-Foundation.mp4', 1, '4241280.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Depilex-Smileagain-Foundation.mp4', 0),
(172, 'DETTOL.mp4', 'DETTOL.mp4', 1, '493440.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/DETTOL.mp4', 0),
(173, 'Dhanak-Clinics.mp4', 'Dhanak-Clinics.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Dhanak-Clinics.mp4', 0),
(174, 'Dhanak-Fashion.mp4', 'Dhanak-Fashion.mp4', 1, '493440.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Dhanak-Fashion.mp4', 0),
(175, 'DHL.mp4', 'DHL.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/DHL.mp4', 0),
(176, 'DING-DONG-BUBBLE.mp4', 'DING-DONG-BUBBLE.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/DING-DONG-BUBBLE.mp4', 0),
(177, 'Drakkar-Noir.mp4', 'Drakkar-Noir.mp4', 1, '4241280.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Drakkar-Noir.mp4', 0),
(178, 'Fanta.mp4', 'Fanta.mp4', 1, '2760960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Fanta.mp4', 0),
(179, 'Gul-Ahmed.mp4', 'Gul-Ahmed.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Gul-Ahmed.mp4', 0),
(180, 'HILAL-FOODS.mp4', 'HILAL-FOODS.mp4', 1, '2035200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/HILAL-FOODS.mp4', 0),
(181, 'Ismail-Industries-Limited.mp4', 'Ismail-Industries-Limited.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Ismail-Industries-Limited.mp4', 0),
(182, 'MITCHELLS.mp4', 'MITCHELLS.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/MITCHELLS.mp4', 0),
(183, 'NATIONAL-FOODS.mp4', 'NATIONAL-FOODS.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/NATIONAL-FOODS.mp4', 0),
(184, 'Nestle-Pakistan.mp4', 'Nestle-Pakistan.mp4', 1, '1411200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Nestle-Pakistan.mp4', 0),
(185, 'NESTLE.mp4', 'NESTLE.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/NESTLE.mp4', 0),
(186, 'Pakola.mp4', 'Pakola.mp4', 1, '2760960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Pakola.mp4', 0),
(187, 'Pepsi.mp4', 'Pepsi.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Pepsi.mp4', 0),
(188, 'Philip-Morris-Pakistan-Ltd.mp4', 'Philip-Morris-Pakistan-Ltd.mp4', 1, '2035200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Philip-Morris-Pakistan-Ltd.mp4', 0),
(189, 'Reckitt-Benckiser.mp4', 'Reckitt-Benckiser.mp4', 1, '4241280.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Reckitt-Benckiser.mp4', 0),
(190, 'Red-Bull.mp4', 'Red-Bull.mp4', 1, '2760960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Red-Bull.mp4', 0),
(191, 'Sana-Safinaz.mp4', 'Sana-Safinaz.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Sana-Safinaz.mp4', 0),
(192, 'SHAN-FOOD.mp4', 'SHAN-FOOD.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/SHAN-FOOD.mp4', 0),
(193, '7up (1).mp4', '7up (1).mp4', 1, '17564734.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (1).mp4', 0),
(194, '7up (2).mp4', '7up (2).mp4', 1, '202548.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (2).mp4', 0),
(195, '7up (3).mp4', '7up (3).mp4', 1, '158012.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (3).mp4', 0),
(196, '7up (4).mp4', '7up (4).mp4', 1, '202061.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (4).mp4', 0),
(197, '7up (5).mp4', '7up (5).mp4', 1, '126108.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (5).mp4', 0),
(198, '7up (6).mp4', '7up (6).mp4', 1, '19974269.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up (6).mp4', 0),
(199, '7 Up Desert Tvc-02 30sec.mov', '7 Up Desert Tvc-02 30sec.mov', 1, '981.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7 Up Desert Tvc-02 30sec.mov', 0),
(200, '7 Up tvc-11 (2007) redo.mov', '7 Up tvc-11 (2007) redo.mov', 1, '899.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7 Up tvc-11 (2007) redo.mov', 0),
(201, '7 Up tvc-11 (2007).mov', '7 Up tvc-11 (2007).mov', 1, '914.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7 Up tvc-11 (2007).mov', 0),
(202, '7up tape-01 (Desert tvc-03) 35sec.mp4', '7up tape-01 (Desert tvc-03) 35sec.mp4', 1, '3600000.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-03) 35sec.mp4', 0),
(203, '7up tape-01 (Desert tvc-04) 05sec.mp4', '7up tape-01 (Desert tvc-04) 05sec.mp4', 1, '1317120.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-04) 05sec.mp4', 0),
(204, '7up tape-01 (Desert tvc-29) 28sec.mp4', '7up tape-01 (Desert tvc-29) 28sec.mp4', 1, '3686400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-29) 28sec.mp4', 0),
(205, '7up tape-01 (Desert tvc-30) 28sec.mp4', '7up tape-01 (Desert tvc-30) 28sec.mp4', 1, '3538560.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-30) 28sec.mp4', 0),
(206, '7 Up Desert Tvc-02 30sec.mov', '7 Up Desert Tvc-02 30sec.mov', 1, '981.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7 Up Desert Tvc-02 30sec.mov', 0),
(207, '7 Up tvc-11 (2007) redo.mov', '7 Up tvc-11 (2007) redo.mov', 1, '899.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7 Up tvc-11 (2007) redo.mov', 0),
(208, '7 Up tvc-11 (2007).mov', '7 Up tvc-11 (2007).mov', 1, '914.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7 Up tvc-11 (2007).mov', 0),
(209, '7up tape-01 (Desert tvc-03) 35sec.mp4', '7up tape-01 (Desert tvc-03) 35sec.mp4', 1, '3600000.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-03) 35sec.mp4', 0),
(210, '7up tape-01 (Desert tvc-04) 05sec.mp4', '7up tape-01 (Desert tvc-04) 05sec.mp4', 1, '1317120.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-04) 05sec.mp4', 0),
(211, '7up tape-01 (Desert tvc-29) 28sec.mp4', '7up tape-01 (Desert tvc-29) 28sec.mp4', 1, '3686400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-29) 28sec.mp4', 0),
(212, '7up tape-01 (Desert tvc-30) 28sec.mp4', '7up tape-01 (Desert tvc-30) 28sec.mp4', 1, '3538560.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-30) 28sec.mp4', 0),
(213, '7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', '7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', 0),
(214, '7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', '7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', 0),
(215, '7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', '7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', 0),
(216, '7up tape-01 (Fido tvc-07) 38sec.mp4', '7up tape-01 (Fido tvc-07) 38sec.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec.mp4', 0),
(217, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', 0),
(218, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', 0),
(219, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', 0),
(220, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', 0),
(221, '7up tape-01 (Fido tvc-19) 45sec.mp4', '7up tape-01 (Fido tvc-19) 45sec.mp4', 1, '4241280.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-19) 45sec.mp4', 0),
(222, '7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', '7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', 1, '2035200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', 0),
(223, '7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', '7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', 1, '2035200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', 0),
(224, '7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', '7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', 1, '1036800.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', 0),
(225, '7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', '7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', 1, '1036800.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', 0),
(226, '7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', '7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', 1, '1036800.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', 0),
(227, '7up tape-01 (Fido tvc-21) 10sec.mp4', '7up tape-01 (Fido tvc-21) 10sec.mp4', 1, '1036800.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec.mp4', 0),
(228, '7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', '7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', 1, '493440.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', 0),
(229, '7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', '7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', 1, '493440.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', 0),
(230, 'Abbott.mp4', 'Abbott.mp4', 1, '1545600.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Abbott.mp4', 0),
(231, 'AIDS.mp4', 'AIDS.mp4', 1, '1545600.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/AIDS.mp4', 0),
(232, 'Ansar-Burney-Trust.mp4', 'Ansar-Burney-Trust.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Ansar-Burney-Trust.mp4', 0),
(233, 'Audi-Cars.mp4', 'Audi-Cars.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Audi-Cars.mp4', 0),
(234, 'B.P.mp4', 'B.P.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/B.P.mp4', 0),
(235, 'BAIT-UL-SUKOON.mp4', 'BAIT-UL-SUKOON.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/BAIT-UL-SUKOON.mp4', 0),
(236, 'BARBIE.mp4', 'BARBIE.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/BARBIE.mp4', 0),
(237, 'Barclays.mp4', 'Barclays.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Barclays.mp4', 0),
(238, 'BATA.mp4', 'BATA.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/BATA.mp4', 0),
(239, 'Bonanza.mp4', 'Bonanza.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Bonanza.mp4', 0),
(240, 'BRAUN.mp4', 'BRAUN.mp4', 1, '4801920.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/BRAUN.mp4', 0),
(241, 'Butterfly.mp4', 'Butterfly.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Butterfly.mp4', 0),
(242, 'Capstan-Cigarette.mp4', 'Capstan-Cigarette.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Capstan-Cigarette.mp4', 0),
(243, 'CASTROL.mp4', 'CASTROL.mp4', 1, '1545600.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/CASTROL.mp4', 0),
(244, 'Cheetos.mp4', 'Cheetos.mp4', 1, '1411200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Cheetos.mp4', 0),
(245, 'CITIBANK.mp4', 'CITIBANK.mp4', 1, '1411200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/CITIBANK.mp4', 0),
(246, 'Citizens-Foundation.mp4', 'Citizens-Foundation.mp4', 1, '1176960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Citizens-Foundation.mp4', 0),
(247, 'Coca-Cola.mp4', 'Coca-Cola.mp4', 1, '1411200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Coca-Cola.mp4', 0),
(248, 'Colgate.mp4', 'Colgate.mp4', 1, '1176960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Colgate.mp4', 0),
(249, 'Cricket.mp4', 'Cricket.mp4', 1, '1176960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Cricket.mp4', 0),
(250, 'Dalda-Foods.mp4', 'Dalda-Foods.mp4', 1, '1176960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Dalda-Foods.mp4', 0),
(251, 'Depilex-Smileagain-Foundation.mp4', 'Depilex-Smileagain-Foundation.mp4', 1, '4241280.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Depilex-Smileagain-Foundation.mp4', 0),
(252, 'DETTOL.mp4', 'DETTOL.mp4', 1, '493440.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/DETTOL.mp4', 0),
(253, 'Dhanak-Clinics.mp4', 'Dhanak-Clinics.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Dhanak-Clinics.mp4', 0),
(254, 'Dhanak-Fashion.mp4', 'Dhanak-Fashion.mp4', 1, '493440.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Dhanak-Fashion.mp4', 0),
(255, 'DHL.mp4', 'DHL.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/DHL.mp4', 0),
(256, 'DING-DONG-BUBBLE.mp4', 'DING-DONG-BUBBLE.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/DING-DONG-BUBBLE.mp4', 0),
(257, 'Drakkar-Noir.mp4', 'Drakkar-Noir.mp4', 1, '4241280.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Drakkar-Noir.mp4', 0),
(258, 'Fanta.mp4', 'Fanta.mp4', 1, '2760960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Fanta.mp4', 0),
(259, 'Gul-Ahmed.mp4', 'Gul-Ahmed.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Gul-Ahmed.mp4', 0),
(260, 'HILAL-FOODS.mp4', 'HILAL-FOODS.mp4', 1, '2035200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/HILAL-FOODS.mp4', 0),
(261, 'Ismail-Industries-Limited.mp4', 'Ismail-Industries-Limited.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Ismail-Industries-Limited.mp4', 0),
(262, 'MITCHELLS.mp4', 'MITCHELLS.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/MITCHELLS.mp4', 0),
(263, 'NATIONAL-FOODS.mp4', 'NATIONAL-FOODS.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/NATIONAL-FOODS.mp4', 0),
(264, 'Nestle-Pakistan.mp4', 'Nestle-Pakistan.mp4', 1, '1411200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Nestle-Pakistan.mp4', 0),
(265, 'NESTLE.mp4', 'NESTLE.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/NESTLE.mp4', 0),
(266, 'Pakola.mp4', 'Pakola.mp4', 1, '2760960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Pakola.mp4', 0),
(267, 'Pepsi.mp4', 'Pepsi.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Pepsi.mp4', 0),
(268, 'Philip-Morris-Pakistan-Ltd.mp4', 'Philip-Morris-Pakistan-Ltd.mp4', 1, '2035200.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Philip-Morris-Pakistan-Ltd.mp4', 0),
(269, 'Reckitt-Benckiser.mp4', 'Reckitt-Benckiser.mp4', 1, '4241280.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Reckitt-Benckiser.mp4', 0),
(270, 'Red-Bull.mp4', 'Red-Bull.mp4', 1, '2760960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Red-Bull.mp4', 0),
(271, 'Sana-Safinaz.mp4', 'Sana-Safinaz.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Sana-Safinaz.mp4', 0),
(272, 'SHAN-FOOD.mp4', 'SHAN-FOOD.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/SHAN-FOOD.mp4', 0),
(273, 'SHEZAN.mp4', 'SHEZAN.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/SHEZAN.mp4', 0),
(274, 'Sprite.mp4', 'Sprite.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Sprite.mp4', 0),
(275, 'Sting.mp4', 'Sting.mp4', 1, '5030400.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Sting.mp4', 0),
(276, 'UFONE.mp4', 'UFONE.mp4', 1, '2760960.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/UFONE.mp4', 0),
(277, 'Unilever.mp4', 'Unilever.mp4', 1, '3546240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/Unilever.mp4', 0),
(278, 'ZONG.mp4', 'ZONG.mp4', 1, '1545600.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido/ZONG.mp4', 0),
(279, '7UP Fido Prom-01 35sec.mp4', '7UP Fido Prom-01 35sec.mp4', 1, '4608000.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido Promo/7UP Fido Prom-01 35sec.mp4', 0),
(280, '7UP Fido Prom-01 36sec.mp4', '7UP Fido Prom-01 36sec.mp4', 1, '4266240.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up Fido Promo/7UP Fido Prom-01 36sec.mp4', 0),
(281, '7up.mov', '7up.mov', 1, '221983.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/7up.mov', 0),
(282, 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar.mp', 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar.mp4', 1, '21575053.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar.mp4', 0),
(283, 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar1.m', 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar1.mp4', 1, '21575053.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar1.mp4', 0),
(284, 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar2.m', 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar2.mp4', 1, '21575053.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar2.mp4', 0),
(285, 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar3.m', 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar3.mp4', 1, '21575053.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar3.mp4', 0),
(286, 'PANIYON_SA_Full_Song_¦_Satyameva_Jayate_¦_John_Abraham_¦_Aisha_Sharma_', 'PANIYON_SA_Full_Song_¦_Satyameva_Jayate_¦_John_Abraham_¦_Aisha_Sharma_¦_Tulsi_Kumar_¦_Atif_Aslam.mp4', 1, '206425.000', '1.000', 1, 1, 1, '5', '2018-11-08', '1', '100.000', 'uploads\\7up/PANIYON_SA_Full_Song_¦_Satyameva_Jayate_¦_John_Abraham_¦_Aisha_Sharma_¦_Tulsi_Kumar_¦_Atif_Aslam.mp4', 0);

-- --------------------------------------------------------

--
-- Table structure for table `agency`
--

CREATE TABLE `agency` (
  `AgencyID` int(4) NOT NULL COMMENT 'Agency ID',
  `AgencyName` varchar(50) NOT NULL COMMENT 'Agency Name',
  `AgencyDetail` varchar(200) DEFAULT NULL COMMENT 'Agency Detail',
  `agency_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agency`
--

INSERT INTO `agency` (`AgencyID`, `AgencyName`, `AgencyDetail`, `agency_image`) VALUES
(1, 'Synergy', 'Marketing & Advertising', 'synergy.jpeg'),
(2, 'Adcom Leo Burnett', 'Adcom, one of Pakistan’s largest independent advertising agencies, recently announced its affiliation with Leo Burnett', 'adcom11.png'),
(3, 'RED Communication Arts', 'n 1996, RED Communication Arts began as an agency with a new belief. A belief that contemporary advertising and communications', 'redComm1.png'),
(4, 'LOWE & RAUF', 'One of the pioneers and leading advertising agencies of Pakistan with 17 clients in total, including such names as NESTLE, Unilever and Dalda etc', 'lowe1.png'),
(5, '5iCreations', '5iCreations (Pvt.) Ltd. has been successfully providing solutions to Large and Small Medium Sized Businesses all over the world specially United States', 'icreation1.png'),
(6, 'Interflow Communications', 'For almost three decades, Interflow Communications has been one of the pioneers of marketing communication services in Pakistan. With offices in Karachi, Lahore and Islamabad Interflow Communications', 'intfr.png'),
(7, 'Contact Plus', 'Contact Plus is Pakistan’s Biggest Marketing Agency which provides all types of BTL (Below the line) marketing services and facilities including consumer activation, road shows event celebration, DDS,', 'contact+1.png'),
(8, 'Argus Advertising', 'In 1978, Argus Advertising was set up as a full service advertising agency, based in Karachi on I. I. Chundrigar Road. Over three decades, the agency has grown into a group of companies that provide a', 'argus1.png'),
(9, 'Mass Advertising', 'Founded in 1973, Mass Advertising Specializes in comprehensive brand management through conventional and non-conventional communication media', 'mass-logo1.png'),
(10, 'ECHO Digital Marketing', 'They are a group of enthusiasts who are totally dedicated to providing social strategy management and community building for their clients', 'echo.png'),
(11, 'PIRANA GROUP', 'A communications agency that offers customized solutions that help build clients brands and businesses. They offer tailor made proposals with a focus on effectiveness', 'pirana.png'),
(12, 'Orient Advertising', 'Established in 1953, Orient Advertising is Pakistan’s largest advertising agency for the last 22 years', 'orient1.png');

-- --------------------------------------------------------

--
-- Table structure for table `artistprofile`
--

CREATE TABLE `artistprofile` (
  `ArtistID` int(4) NOT NULL COMMENT 'Artist ID Unique',
  `Name` varchar(50) NOT NULL COMMENT 'Artist Name',
  `LastName` varchar(50) DEFAULT NULL COMMENT 'Last Name',
  `Gender` varchar(10) DEFAULT NULL COMMENT 'Gender',
  `Age` int(3) DEFAULT NULL COMMENT 'Age',
  `Rate` int(5) DEFAULT NULL COMMENT 'Rate Per Min',
  `Role` varchar(50) DEFAULT NULL COMMENT 'Role Play by Actor',
  `Description` varchar(200) DEFAULT NULL COMMENT 'Statment/Remarks',
  `artist_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artistprofile`
--

INSERT INTO `artistprofile` (`ArtistID`, `Name`, `LastName`, `Gender`, `Age`, `Rate`, `Role`, `Description`, `artist_image`) VALUES
(1, 'Aiman Khan', 'M. Khan', 'F', 30, 4, 'Actress & Model', 'Aiman Khan is one of the sensations in Pakistan', 'aimankhan.jpg'),
(2, 'Hina Altaf', 'Khan', 'F', 25, 4, 'Vj, Host, and Mode', 'Hina Altaf is Pakistani Vj, Host, and Mode', 'hinaaltaf.jpg'),
(3, 'Iqra Aziz', 'Khan', 'F', 23, 5, 'Actress', 'Personal life, Family, and Mothe', 'iqra_aziz_246.jpeg'),
(4, 'Diya Mughal', 'Mughal', 'F', 32, 4, 'Model, Actress', 'Pakistani Model', 'Diya-Mughal.jpg'),
(5, 'Aliya', 'Ali', 'F', 29, 3, 'Actress & Model', 'Pakistani Celebrities  ', 'Aliya-Ali-Pics.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `BrandID` int(4) NOT NULL COMMENT 'Brand ID',
  `BrandName` varchar(30) NOT NULL COMMENT 'Brand Name',
  `Description` varchar(200) DEFAULT NULL COMMENT 'Brand Description',
  `image` text NOT NULL,
  `SectorID` int(4) NOT NULL DEFAULT '36'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`BrandID`, `BrandName`, `Description`, `image`, `SectorID`) VALUES
(1, 'Unilever', 'Selling fast-moving consumer goods', 'unilever.png', 10),
(2, 'Gul-Ahmed', 'Gul Ahmed is a preeminent Pakistani textile provider in the market', 'gulahmed-logo.png', 29),
(3, 'Sana-Safinaz', 'Sana Safinaz is the name of the fashion icon in Pakistan', 'sana_safinaz.jpg', 29),
(4, 'Bonanza', 'Pakistani Clothing Brands', 'bonanza-clothing-brand-logo.png', 29),
(5, 'NESTLE', 'Nestle Pakistan is the best leading food company in Pakistan', 'nestle.jpg', 10),
(6, 'Pepsi', 'PEPSI COLA PAKISTAN Soft Drinks', 'Pepsi.png', 10),
(7, 'MITCHELLS', 'Mitchell’s Fruit Farm Limited is the oldest food company of Pakistan that is established by a Francis J', 'mitchelles.png', 10),
(8, 'NATIONAL-FOODS', 'National Foods Limited is a multinational company which started its career in 1970 and initially it was a spice company', 'National-food-logo.jpg', 10),
(9, 'SHAN-FOOD', 'Shan Food Industries is a spice company of Pakistan. It was established in 1981 on very small scale and after few years of its establishment it became a brand and started to export its products to oth', 'shan_food.png', 10),
(10, 'SHEZAN', 'Shezan International Limited is a Pakistani beverage manufacturing company. This company is famous for its best quality fruits and vegetable', 'Shezan.JPG', 10),
(12, '7Up', 'Another product of Pepsico, 7up enjoys its fair share of Pakistani consumers. The lemon flavour makes it very popular particularly with barbeque', '7Up.jpg', 10),
(13, 'Coca-Cola', 'Another popular drink in Pakistan, Coca-Cola has for long been consumed', 'Coca-Cola.png', 10),
(14, 'Sprite', 'A product of Coca Cola, Sprite competes with 7up, having a similar flavour', 'Sprite.png', 10),
(15, 'Sting', 'Marketed as an energy drink, Sting has hit the Pakistani market like a storm ever since its arrival. Among the best selling products of Pepsico', 'Sting.png', 10),
(16, 'Fanta', 'The soft orange flavor makes Fanta one of the favorite cold drinks among Pakistanis. A product of Coca Cola', 'Fanta.png', 10),
(17, 'Pakola', 'Pakola has been one of the most popular local brands in Pakistan', 'PAKOLA.png', 10),
(18, 'Red-Bull', 'An energy drink by trait, Red bull is still a casual drink consumed particularly by the youngsters in Pakistan', 'Red-Bull.png', 10),
(20, 'UFONE', 'Pakistan Telecommunication Company Limited.', 'Ufone.jpg', 28),
(21, 'ZONG', 'CMPak envisions to enable a fully connected environment for Pakistanis by leveraging the technological edge in 4G LTE', 'ZONG.jpg', 36),
(22, 'Abbott', 'The global healthcare company that conducts innovative research and manufactures products for human health', 'abbott.jpg', 22),
(23, 'AIDS', 'HIV is recognized as a health concern in Pakistan with the number of cases growing. Moderately high drug use and lack of acceptance that non-marital sex is common in the society have allowed the HIV e', 'AIDS.jpg', 36),
(24, 'Ansar-Burney-Trust', 'Ansar Burney is a leading Pakistani human and civil rights activist ', 'Ansar-Burney-Trust.png', 36),
(25, 'Audi-Cars', 'Audi worldwide. Models, products and ... High-performance sports car with electric drive ,Learn about what drives the Audi Brand', 'audi-cars.jpg', 36),
(26, 'B.P', 'B.P. Industries (Pvt) Ltd., leverage your professional network, and get hired. ... Consumer Goods', 'bp.jpg', 10),
(27, 'BAIT-UL-SUKOON', 'Bait-ul-Sukoon’ (House of Peace & Contentment) is the only Free cancer hospital with a hospice in Pakistan. Baitul-ul Sukoon is located in Karachi.', 'BAIT-UL-SUKOON.png', 36),
(28, 'BARBIE', 'Barbie is a fashion doll manufactured by the American toy-company Mattel, Inc. and launched in March 1959. Barbie is the figurehead of a brand of Mattel dolls and accessories, including other family m', 'BARBIE.jpg', 36),
(29, 'Barclays', 'British multinational investment bank and financial services company headquartered in London. Apart from investment banking, Barclays is organised into four core businesses: personal banking, corporat', 'Barclays.png', 13),
(30, 'BATA', 'Bata Pakistan offers quality footwear in the latest styles. Buy shoes online for men, women & kids', 'BATA.png', 36),
(31, 'BRAUN', 'Braun is a renowned name and is famous for making top-end products like Epilators, Hair Dryers, Tooth brushes, Trimmer, Straighter and Kitchen products like hand blender.', 'braun.jpg', 36),
(32, 'Butterfly', 'Very first sanitary napkin launched in the nation. Butterfly Mother Comforts has catered needs of women for over two decades. Giving every woman Protection', 'butterfly-body-liners-logo-header_200x100.png', 36),
(33, 'Ismail-Industries-Limited', 'Ismail Industries Limited is one of the largest food companies in Pakistan, ... Candyland is the leading confectionery company in Pakistan', 'Ismail_Industries.png', 10),
(35, 'Capstan-Cigarette', 'Pakistan Tobacco Company Limited is a part of British American Tobacco plc, one of the world’s most international businesses, with brands sold in more than 200 markets around the world', 'CAPSTAN.jpeg', 36),
(36, 'CASTROL', 'Castrol - Engine Oil and Lubricants | Synthetic Oil | Diesel Oil', 'castrol.png', 36),
(37, 'Nestle-Pakistan', 'Good Food Good Life,Enhancing quality of life and contributing to a healthier future', 'nestle.jpg', 10),
(38, 'Cheetos', 'Cheetos is a brand of cheese-flavored puffed cornmeal snacks made by Frito and became a subsidiary of The Pepsi-Cola Company, forming PepsiCo, the current owner of the Cheetos brand Pakistan Cheetos a', 'Cheetos.png', 10),
(39, 'CITIBANK', 'Citibank is the consumer division of financial services multinational Citigroup', 'Citibank.png', 7),
(40, 'Citizens-Foundation', 'The Citizens Foundation is a non-profit organization, and one of the largest privately owned networks of low-cost formal schools in Pakistan. The Foundation operates a network of 1,482 school units, e', 'CITIZEN_FOUNDATION.png', 36),
(41, 'Cricket', 'Pakistan Cricket Board (PCB) controls and organises all tours and matches undertaken by the Pakistan national cricket team Shaheens (Eagles), Men in Green, Green Shirts', 'CRICKET.jpg', 36),
(42, 'Colgate ', 'Colgate-Palmolive (Pakistan) Limited is engaged in the manufacture and sale of detergents, personal care and other related products. The Company\'s principal classes of products include Personal Care, ', 'Colgate.png', 36),
(43, 'Dalda-Foods', 'Dalda Cooking Oil. Dalda is the first brand to bring premium cooking oil for its consumers, conforming to international quality standards. ... Apart from the perfect blend of Dalda Cooking Oil, Dalda\'', 'DALDA.png', 10),
(44, 'Drakkar-Noir', 'Drakkar Noir is a men\'s fragrance by Guy Laroche created by perfumer Pierre Wargnye. The fragrance was introduced in 1982 and is manufactured under license by the L\'Oréal Group', 'drakkar_noir.jpg', 36),
(45, 'Depilex-Smileagain-Foundation', 'Depilex Smileagain Foundation is committed to provide the acid survivors not only with medical care and assistance but also an adequate chance to become productive, self-reliant members of the society', 'DEPILEX_SMILEAGAIN_FOUNDATION.jpg', 36),
(46, 'Reckitt-Benckiser', 'Reckitt Benckiser Group plc (RB) is a British multinational consumer goods company ... RB\'s brands include the antiseptic brand Dettol', 'Reckitt-Benckiser-logo.png', 22),
(47, 'DETTOL', 'Dettol Antiseptic Liquid Disinfectant is the name of commercial liquid and solid antiseptic products belonging to a household product line manufactured by Reckitt Benckiser', 'Dettol_logo.jpg', 22),
(48, 'Dhanak-Fashion', 'Dhanak is one of the fastest growing fashion brands in Pakistan', 'dhanak.jpg', 29),
(49, 'Dhanak-Clinics', 'Dhanak Health Care Centres promoting reproductive health of women,Most of the clinics opened nation-wide are renovations of existing clinics, however some of them are newly constructed. The experience', 'dhanak_CLINICS.jpg', 36),
(50, 'DHL', 'International Shipping, Parcel Delivery Services,DHL Express shares its DHL brand with business units such as DHL Global Forwarding and DHL Supply Chain', 'Dhl.png', 33),
(51, 'DING-DONG-BUBBLE', 'Ding Dong – Everyone’s favorite bubble gum! Its scrumptious tutti-frutti and strawberry taste has captured the hearts of children all over the nation', 'Ding_Dong_bubble.png', 10),
(52, 'HILAL-FOODS ', 'Hilal Foods Pvt. Ltd. is one of the leading confectionery and food manufacturing companies of Pakistan and exporting to more than 20 countries around the world. ', 'Hilal.jpg', 10),
(53, 'Philip-Morris-Pakistan-Ltd', 'Philip Morris (Pakistan) Limited (PMPKL)', 'Philip_Morris_Pakistan.jpg', 36),
(54, 'DuPont-Pakistan', 'DuPont is working inclusively with others to find innovative, science-enabled solutions to some of the world\'s biggest challenges.', 'dupont.jpg', 5),
(55, 'DURAFOAM', 'Durafoam proudly manufactures quality mattresses to make sure you experience the best of comfort. We ensure durability and longevity of the product life under normal usage.', 'Durafoam.jpg', 5),
(56, 'DURACELL', 'Duracell, recently acquired by Warren Buffet’s Berkshire Hathaway, is the number one preferred battery brand globally and is among the world’s top 100 brands', 'Duracell.png', 8),
(57, 'English-Biscuit-Manufacturers ', 'In each bite of any delicious Peek Freans biscuit lies the rich tradition of true grit and hard work that has been so essential to EBM’s journey. ', 'English_biscuit_manufacturers.jpg', 10),
(58, 'Editorial-Everest-S.A', 'Editorial Everest S.A. publishes books. The company was founded in 1975 and is based in Madrid, Spain. As of September 7, 2018, Editorial Everest S.A. operates as a subsidiary of Ediciones Nobel S.A.', 'Editorial_Everest_S.jpg', 36),
(59, 'Embassy-Cigarette', 'Embassy is a British brand of cigarettes, currently owned and manufactured by Imperial Tobacco', 'Embassy_Cigarette.jpg', 32),
(60, 'Emirates-Airline', 'The Emirates story started in 1985 when we launched operations with just two aircraft.Today, we fly the world’s biggest fleets of Airbus A380s and Boeing 777s', 'Emirates_Airline.png', 33),
(61, 'Engro-Food', 'Engro Foods will create & lead a White Revolution in Pakistan, to provide safe, healthy & affordable nutrition to Pakistan', 'Engro_Food.jpg', 10),
(62, 'Engro-Fertilizers', 'Engro Fertilizers Limited is a subsidiary of Engro Corporation and a renowned name in Pakistan’s fertilizer industry.', 'Engro-Fertilizer.png', 9),
(63, 'Engro-Energy', ' Engro Energy is Engro Corp’s first initiative into the country’s power sector.', 'Engro_Energy.png', 8),
(64, 'FFC-Fauji-Fertilizer-Company', 'Fauji Fertilizer Company Limited (The Company of Punjab)Fauji Fertilizer Company logo.svgTraded asPSX: FFCIndustryChemicalsFounded1978HeadquartersRawalpindi, PakistanNumber of locationsRawalpindi (Hea', 'FFC_Fauji_Fertilizer_Company.jpg', 9),
(65, 'Fauji-Foundation', 'Fauji Foundation is one of the largest financial services and one of the largest energy conglomerates in Pakistan, with interests in fertilizer, cement, food, power generation, gas exploration', 'Fauji_Foundation.png', 17),
(66, 'FAUJI-CEREALS-Food', 'Fauji Cereals was established in 1954, It is the major producer of breakfast cereals in Pakistan with wide range of products. It is Pakistan’s most prestigious and biggest cereal processing unit The C', 'FAUJI_CEREALS.jpg', 10),
(67, 'HEAD-LINE', 'Breaking News, Latest Pakistan News & Updates', 'Headlines_Today_logo_svg.png', 28),
(68, 'HABIB-BANK-HBL', 'Habib Bank Limited d/b/a HBL is a Karachi based multinational bank. It is the largest bank by assets in Pakistan. Founded in 1941, HBL became Pakistan\'s first commercial bank. In 1951', 'HABIB_BANK_HBL.jpg', 7),
(69, 'Santex-Products -Butterfly', ' Pakistani woman living in the 80s and 90s, chances are you were ... However, in 2015, Santex Products (owners of the Butterfly brand) launched a new ... between the ages of 15 to 40, only 30% use bra', 'Santex_Products_(Butterfly).jpg', 10),
(70, 'Canon ', 'Canon Inc is a Japanese multinational corporation specializing in the manufacture of imaging and optical products, including cameras, camcorders, photocopiers, steppers, computer printers and medical ', 'Canon.png', 28),
(71, 'Php', 'PHP: Hypertext Preprocessor (or simply PHP) is a server-side scripting language designed for Web development, and also used as a general-purpose programming language. It was originally created by Rasm', 'php.jpg', 36),
(72, 'ACCP', 'scascas', '5a393165c488ac6062ac2add.png', 36);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `login_id` int(11) NOT NULL,
  `login_name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`login_id`, `login_name`, `password`) VALUES
(1, 'admin@gmail.com', '123');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `ProductID` int(4) NOT NULL COMMENT 'Product ID',
  `ProductName` varchar(50) NOT NULL COMMENT 'Product Name',
  `ProductDetail` varchar(200) DEFAULT NULL COMMENT 'Product Detail',
  `BrandID` int(4) DEFAULT NULL COMMENT 'Product Link With Brand',
  `pimage` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`ProductID`, `ProductName`, `ProductDetail`, `BrandID`, `pimage`) VALUES
(1, 'Brooke Bond', 'Since 1869, Brooke Bond has brought you the perfect tasting tea experience', 1, 'brooke_bond.png'),
(2, 'Knorr', 'Knorr is a food brand with a wide range of soups, stock cubes, bouillons, seasonings and sauce', 1, 'knor.png'),
(3, 'Lipton', 'Sir Thomas Lipton. In an age when tea was a rare and expensive luxury', 1, 'lipton-logo-vector-xs.png'),
(4, 'Pearl Dust', 'In 1869 Arthur Brooke, the son of a tea merchant, opened a tea shop in Manchester.', 1, 'pearldust.gif'),
(5, 'DOVE', 'Dove’s mission is to make women feel beautiful every day by widening the definition of beauty and inspiring them to take greater care of themselves.', 1, 'dove.png'),
(6, 'Blue Band', 'Blue Band and Rama products – from margarine spreads and cooking fats to cream alternatives and cheese spreads', 1, 'Blue_Band.png'),
(7, 'Close Up', 'Launched in 1967, Close Up is a clear gel toothpaste containing a mouthwash and micro whiteners to ensure long-lasting fresh breath and white teeth', 1, 'close_up.gif'),
(8, 'Comfort', 'Comfort is a fabric conditioner that is available globally, with numerous variants such as Comfort Easy Iron and Comfort Bright Whites. It is also sold under local names, such as Drive and Yumo? in so', 1, 'Comfort_logo.png'),
(9, 'Lifebuoy Soap', 'Lifebuoy aims to provide affordable and accessible hygiene solutions to enable a life free from hygiene related worries, everywhere.', 1, 'Lifebuoy.jpg'),
(10, 'Lifebuoy Shampoo', 'With the power of Milk Protein, it nourishes hair from root to tip, making it long on the outside and strong on the inside', 1, 'lifebuoyshampoo-logo-500x500.jpg'),
(11, 'LUX', 'Lux is a global brand, launched in 1925, whose range of products includes beauty soaps, shower gels, bath additives, shampoos and conditioners of the stars', 1, 'Lux_logo.jpg'),
(12, 'CLEAR', 'Clear was launched in 1979 as a haircare product to tackle problem hair, notably hair with dandruff. Its breakthrough technology', 1, 'Clear_2007.png'),
(13, 'MAGNUM  ', 'Launched in 1989, Magnum was the first hand-held ice cream created as a premium ice cream for adults. Today, Magnum is a leading global brand, selling 1 billion units annually', 1, 'Magnum_logo_2006.png'),
(14, 'Cornetto', 'Cornetto has been for decades one the world\'s most famous and popular ice-creams. Cornetto\'s soft creamy core, crunchy wafer, and delicious chocolate', 1, 'cornetto.png'),
(15, 'Domex', 'Domex  is a powerful, quick-acting bleach', 1, 'domestos_large.png'),
(16, 'Pepsodent', 'Pespodent is a leading oral care brand with a wide range of products', 1, 'logo-pepsodent-png-5.png'),
(17, 'Fair & Lovely', 'Fair & Lovely is a best-selling fairness cream. The product contains no bleach or harmful ingredients', 1, 'fair-lovely.jpg'),
(18, 'Pond’s', 'Pond’s is a skincare brand sold in an affordable price bracket. The brand has a long heritage', 1, 'ponds.jpg'),
(19, 'Fruttare', 'Fruttare’s mission is to inspire the freedom to indulge by providing pleasure and health through the natural goodness of fruit without too many calories', 1, 'fruttare.png'),
(20, 'Rin', 'Rin is a detergent brand with a range of products including washing powder, detergent bars and a clothes whitening liquid.', 1, 'Rin-logo.jpg'),
(21, 'Sunsilk', 'Sunsilk is a hair-care brand for women. It is the world’s leading brand in hair conditioning and the second largest in shampoo', 1, 'sunsilk.jpg'),
(22, 'Glaxose-D', 'Glaxose-D is a glucose-based energy drink and the original energy drink in Pakistan. It is a non-flavoured energy drink, used both by children and adults.', 1, 'glaxosed.png'),
(23, 'Rafhan', 'Rafhan, one of the biggest food brands in Unilever Pakistan, offers a wide range of products, from desserts to corn oil and cornflour.', 1, 'rafhan.png'),
(24, 'Surf Excel', 'Surf Excel (also known as Surf in many parts of the world) is a brand of laundry detergent with an accompanying range of laundry products.', 1, 'surf-excel.jpg'),
(25, 'Wall’s', 'Wall\'s is part of Unilever\'s Heartbrand family of ice creams that are sold in more than 40 countries around the world under many different local names', 1, 'Walls_Logo_svg.png'),
(26, 'Animal Nutrition & Disease Prevention', 'With in-feed animal nutrition to bio-security solutions, DuPont is helping to meet the exponentially increasing demand', 54, 'Animal_Logo_grande.png'),
(28, 'Seed', 'DuPont Pioneer is delivering solutions to help meet the needs of a growing', 54, 'seeds.jpg'),
(29, 'DURA COOL AIR', 'Dura CoolAir draws moisture & heat away from your body. The airflow technology provides breathability so you can enjoy more refreshing sleep. Great value and quality.', 55, 'duracool-airconditioning.jpg'),
(30, 'DURA POSTUREPEDIC', 'The firm support provides joint solution for backache and spinal support. The surface helps distribute body weight evenly, helping alleviate back pain and is referred by Orthopedic Specialists to cure', 55, 'dura_posturepedic_-_a.jpg'),
(31, 'DURA SPINE PLUS', 'Dura SpinePlus allows you to enjoy a neutral spine position, keeping your body straighter and releasing pressure off your bones. The sumptuous fabric adds to the beauty and comfort of the mattress', 55, 'dura_spine_plus_-_a.jpg'),
(32, 'DURA SPINE PLUS', 'Dura SpinePlus allows you to enjoy a neutral spine position, keeping your body straighter and releasing pressure off your bones. The sumptuous fabric adds to the beauty and comfort of the mattress', 55, 'dura_spine_plus_-_a1.jpg'),
(33, 'DURA TOP', 'Dura Top allows you to enjoy a unique blend of comfort and luxury. This mattress offers great value for money. The surface enables better night for better days', 55, 'dura_top2_1.jpg'),
(34, 'Ultra Luxury', 'The Ultra Luxury mattress assures best comfort. Allowing affordable luxury, this mattress comes in a variety of sizes to choose from', 55, 'dura_ultra_luxury_-_a.jpg'),
(35, 'Ultra Luxury Firm', 'The Ultra Luxury Firm provides a medium firm core which helps distribute body weight evenly and alleviates back pain and soreness. ', 55, 'luxury_firm_-_a.jpg'),
(36, 'Dura Luxury', 'Soft yet durable, the Dura Luxury is the ultimate sleep luxury of a sleeper. Dwell into a goodnight\'s sleep with this mattress\'s optimum surface', 55, 'dura_luxury_1.jpg'),
(37, 'Dura Spring Ultra', 'The bonnel spring technology enables extra support to support your body in any sleeping position. The mattress is a masterful blend of comfort & durability;', 55, 'dura_spring_ultra_-_a_3.jpg'),
(38, 'Duracell Battery', 'Most commonly used for toys, remote controls, flashlights, calculators, clocks and radios, portable Electronics, wireless mice and keyboards as well as medical devices', 56, 'Duracell.jpg'),
(39, 'SOOPER CLASSIC - Biscuits', '1 selling biscuit brand. Now available in two variants, Egg & Milk and Elaichi cookies, Sooper was launched in 1996. It was re-launched in 2002 in the iconic red packaging', 57, 'sooper.jpg'),
(40, 'SOOPER CLASSIC - Biscuits', '1 selling biscuit brand. Now available in two variants, Egg & Milk and Elaichi cookies, Sooper was launched in 1996. It was re-launched in 2002 in the iconic red packaging', 57, 'sooper1.jpg'),
(41, 'RIO - Biscuits', 'RIO STRAWBERRY & VANILLAPakistan’s favorite cream biscuit, Peek Freans RIO was launched in 1995. ', 57, 'rio.png'),
(42, 'GLUCO Biscuits', 'Tasty and nutritious, Gluco practices what it preaches with its healthy ingredients;  butter, wheat flour, milk and glucose provide energy to high-octane kids, and peace of mind to mothers who want to', 57, 'gluco.png'),
(43, 'Emirates ', 'We inspire travelers around the world with our growing network of worldwide destinations', 60, 'emirates_logo2.gif'),
(44, 'ENERGILE', 'Energile is an instant energy sports drink with energy in the form of Glucose along with Vitamin C and Calcium. Energile makes up for the energy kids consume throughout their daily activities and spor', 1, 'Energiloe.jpg'),
(45, 'Olper\'s Milk', 'Our flagship premium dairy brand, Olper\'s Milk', 61, 'olpers.png'),
(46, 'dairy omung', 'Rise up to a better life with Omung; a pure and nutritious alternate to loose milk that provides the value our consumers', 61, 'dairyomang.jpg'),
(47, 'Tarang', 'Experience sheer bliss in a tea-cup with Tarang, our special tea creamer for that perfect cup of tea.', 61, 'Tarang-Logo-new-Eng.png'),
(48, 'Omore FROZEN DESSERTS', 'Frozen dessert form! Omore', 61, 'Omore_logo.png'),
(49, 'olper\'s cream', 'Enhance the culinary experience with Olper\'s Cream - a rich celebration of life at its creamy best', 61, 'olpers-cream.png'),
(50, 'Olper\'s Tarrka Desi Ghee', 'Olper\'s Tarrka is our premium desi ghee distinct for its pure flavor and aroma. A leading asli desi ghee brand that is extracted from milk, a dash of Tarrka in your favorite cuisine is all it takes to', 61, 'Ghee.jpg'),
(51, 'Olper\'s Dobala', 'Get nutrition and the best value for money with omung dobala', 61, 'dobalaa.jpg'),
(52, 'Engro Urea', 'Nitrogen is first and most important nutrient element required by the plants in larger quantity. Engro is the first company to have setup urea production facility in Pakistan', 62, 'Urea-Eng.png'),
(53, 'Engro DAP', 'Phosphorus is the second important major element required for healthy growth plants and economical yield. Di-Ammonium Phosphate (DAP), which contains 18% nitrogen and 46% Phosphorus', 62, 'DAP-Eng.png'),
(54, 'Engro NP', 'NP formulations that contain Nitrogen and Phosphorus in almost equal quantity have been especially important to Pakistani farmers, given the peculiar deficiency of both components in most of the Pakis', 62, 'NP-Eng.png'),
(55, 'Engro Zarkhez', 'Plants require three major nutrients (i.e. Nitrogen, Phosphorus and Potassium) for quality & higher yield. Zarkhez, introduced in 2002, is the only branded fertilizer in Pakistan', 62, 'Engro_Corporation_svg.png'),
(56, 'EVERYDAY', 'NESTLÉ EVERYDAY, with its heritage of more than 25 years, has established itself as the best tea creaming partner delivering superior cup every time and hence stands as a market leader in tea creaming', 5, 'Everyday_logo_Detail.png'),
(57, 'Sona Urea', 'Urea is a concentrated straight nitrogenous fertilizer that contains 46% nitrogen, which is a major plant nutrient. Nitrogen is a vital component', 64, 'Sona_Urea_Logo.jpg'),
(58, 'Sona DAP & FFC DAP', 'Di-ammonium Phosphate (DAP) belongs to a series of water-soluble ammonium phosphates that is produced through a reaction of ammonia and phosphoric acid. DAP is the most concentrated phosphatic fertili', 64, 'sona_dap.jpg'),
(59, 'FFC SOP', 'SOP is an important source of Potash, a quality nutrient for production of crops, especially fruits and vegetables. FFC SOP contains 50% K2O in addition to 18%', 64, 'ffc.png'),
(60, 'FFC MOP', 'Potassium chloride (commonly referred as Muriate of Potash or MOP) is the most common potassium source used in agriculture, ', 64, 'ffc1.png'),
(61, 'FFC Sona Boron', 'Sona Zinc is a micronutrient fertilizer in the form of Zinc Sulphate Monohydrate (27%) in 3 Kg packing. It is an essential micronutrient ', 64, 'ffc2.png'),
(62, 'Corn Flakes FC', 'Corn Flakes  is Product ofFAUJI CEREALS', 66, 'corn-flakes.png'),
(63, 'Breaking NEWS', 'Breaking NEWS Hourly', 67, 'breakingnews.jpeg'),
(64, 'NEWS Updates', 'Latest NEWS', 67, 'updateNews.png'),
(65, 'Butterfly Pads Thick Large ', 'Thick napkin with high absorb gel Individually wrapped 30% more absorbent Improved quality', 69, 'butterfly-body-liners-logo-header_200x100.png'),
(66, 'Butterfly Breathables', 'Butterfly Breathables Cottony-top sheet napkins provide the silky-smooth feel while absorbing fluid quickly. They are 29 and 32 cm long with breathable microscopic pores for maximum breathability', 69, 'breathable.png'),
(67, 'Butterfly Bumpies', 'Butterfly Bumpies diapers are super absorbent with a soft and stretchy waistband, resealable velcro on each side, an extra retentive core, and a stretchable and fully secure leg cuff to prevent leakag', 69, 'bumpies.jpg'),
(68, 'Candyland', 'CandyLand, currently the largest confectionery company in Pakistan,We are the pioneers in jellies and have  such as lollipops and marshmallows. We take pride in delivering the best quality products an', 33, 'candylandlogo-1.png'),
(69, 'Bisconni', 'Bisconni entered the Pakistani biscuit market with its chocolate filled cookie Cocomo, which over the years has become one of the iconic brands of Pakistan. All products are produced under the Bisconn', 33, 'home-bisconni-img.png'),
(70, 'Canon PowerShot SX540 HS Digital ', 'Characterized by its long-reaching zoom, the Canon PowerShot SX540 HS Digital Camera is also benefitted by the inclusion of a 20.3MP High-Sensitivity CMOS sensor and DIGIC 6 image processor to realize', 70, 'canon_1067c001.jpg'),
(71, 'Canon EOS 1500D DSLR Camera', 'Canon EOS 1500D follows the typical Canon design. Easy to grip and shoot photos with one hand, the ample rubble cladding on the right side also prevents the camera from slipping from your hands even i', 70, 'canon1500dkb.jpg'),
(72, 'Canon EOS 80D Digital SLR Camera ', ' the Canon EOS 80D further clarifies itself using a pair of robust focusing systems and an intuitive design. Featuring a 24.2MP APS-C CMOS sensor and DIGIC 6 image processor, this sleek DSLR is capabl', 70, 'canon_1263c006_eos_80d.jpg'),
(73, 'Barbie Dinning Set', 'Advertise about Barbie Dinning Set', 28, 'barbiesets.jpg'),
(74, 'Barbie Dolls', 'Advertiesment about Barbie Dolls', 28, 'Barbie_Logo.png'),
(75, 'Barbie Water Fun Set', 'Water Fun Set Barbie Set', 28, 'funsets.jpg'),
(77, 'Pepsi', 'Pepsi. Diet Pepsi. Pepsi Zero Sugar. The gang’s all here. Compare flavors, get nutritional facts and check out ingredients for all our Pepsi products.', 6, 'Pepsilogo.png'),
(78, '7Up', '7 Up was created by Charles Leiper Grigg, who launched his St. Louis–based company The Howdy Corporation in 1920. Grigg came up with the formula for a lemon-lime soft drink in 1929.', 12, '7up.png');

-- --------------------------------------------------------

--
-- Table structure for table `roll`
--

CREATE TABLE `roll` (
  `roll_id` int(11) NOT NULL,
  `roll_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roll`
--

INSERT INTO `roll` (`roll_id`, `roll_name`) VALUES
(1, 'Owner'),
(2, 'Admin'),
(3, 'Backoffice');

-- --------------------------------------------------------

--
-- Table structure for table `sector`
--

CREATE TABLE `sector` (
  `SectorID` int(4) NOT NULL COMMENT 'Sector ID',
  `SectorName` varchar(70) NOT NULL COMMENT 'Name of Sector/Industrie',
  `sec_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sector`
--

INSERT INTO `sector` (`SectorID`, `SectorName`, `sec_image`) VALUES
(1, 'AUTOMOBILE ASSEMBLER', 'automobile.jpg'),
(2, 'AUTOMOBILE PARTS & ACCESSORIES', 'automobile_parts.png'),
(3, 'CABLE & ELECTRICAL GOODS', 'electrical1.png'),
(4, 'CEMENT', 'cement1.jpg'),
(5, 'CHEMICAL', 'chemical1.jpg'),
(6, 'CLOSE - END MUTUAL FUND', 'open_end_fund2.png'),
(7, 'COMMERCIAL BANKS', 'commercial_bank.png'),
(8, 'ENGINEERING', 'engineering1.jpg'),
(9, 'FERTILIZER', 'fertilizer1.jpg'),
(10, 'FOOD & DRINKS PERSONAL CARE PRODUCTS', 'personalcare1.jpg'),
(11, 'GLASS & CERAMICS', 'glass_ceramics.jpg'),
(12, 'INSURANCE', 'Insurance-21.jpg'),
(13, 'INV. BANKS / INV. COS. / SECURITIES COS.', 'psx1.png'),
(14, 'JUTE', 'jute1.png'),
(15, 'LEASING COMPANIES', 'Leasing-Companies-Logos1.jpg'),
(16, 'LEATHER & TANNERIES', 'feeling-leather-7301.jpg'),
(17, 'MISCELLANEOUS', 'misc.jpg'),
(18, 'MODARABAS', 'modarabas.png'),
(19, 'OIL & GAS EXPLORATION COMPANIES', 'Oil_and_Gas_Development_Company_Limited_(logo)1.png'),
(20, 'OIL & GAS MARKETING COMPANIES', 'marketing1.png'),
(21, 'PAPER & BOARD', 'logo_ranheimpaper_final.png'),
(22, 'PHARMACEUTICALS', 'Pharmaceuticals.png'),
(23, 'POWER GENERATION & DISTRIBUTION', 'power.png'),
(24, 'REAL ESTATE INVESTMENT TRUST', 'realstate.png'),
(25, 'REFINERY', 'refinery-logo-green.jpg'),
(26, 'SUGAR & ALLIED INDUSTRIES', 'sugar.png'),
(27, 'SYNTHETIC & RAYON', 'synthetic.jpg'),
(28, 'TECHNOLOGY & COMMUNICATION', 'techandcomm.jpg'),
(29, 'TEXTILE Fashion', 'TF-full.jpg'),
(30, 'TEXTILE SPINNING', 'Zagis_Logo_Large_2.png'),
(31, 'TEXTILE WEAVING', 'weaving.jpg'),
(32, 'TOBACCO', 'tobacco.png'),
(33, 'TRANSPORT', 'armi-transport-logo-01-.jpg'),
(34, 'VANASPATI & ALLIED INDUSTRIES', 'PVMA-Logo.jpg'),
(35, 'WOOLLEN', 'woollen-mills.png'),
(36, 'Not Define', 'aimankhan.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE `signup` (
  `signup_id` int(11) NOT NULL,
  `roll_id` int(11) DEFAULT NULL,
  `signup_name` varchar(50) DEFAULT NULL,
  `signup_email` varchar(50) DEFAULT NULL,
  `signup_password` varchar(200) DEFAULT NULL,
  `position` varchar(200) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `program_id` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`signup_id`, `roll_id`, `signup_name`, `signup_email`, `signup_password`, `position`, `status`, `program_id`) VALUES
(430, 3, 'Admin', 'admin@artt.com', '1234', 'Developer', '1', NULL),
(431, 3, 'Sanaullah', 'sanaullah@gmail.com', 'suny', 'Designer', '1', NULL),
(435, 3, 'Ali', 'backoffice@artt.com', '123', 'Designer', '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_last_login`
--

CREATE TABLE `tbl_last_login` (
  `id` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `sessionData` varchar(6144) DEFAULT NULL,
  `machineIp` varchar(3072) DEFAULT NULL,
  `userAgent` varchar(384) DEFAULT NULL,
  `agentString` varchar(3072) DEFAULT NULL,
  `platform` varchar(384) DEFAULT NULL,
  `createdDtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_last_login`
--

INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(1, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"System Administrator\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 14:46:54'),
(2, 1, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"ARTT Owners\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:19:19'),
(3, 9, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Ali\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:20:17'),
(4, 1, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"ARTT Owners\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:21:09'),
(5, 11, '{\"role\":\"4\",\"roleText\":\"Student\",\"name\":\"Saif\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:38:01'),
(6, 1, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"ARTT Owners\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:39:38'),
(7, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:47:18'),
(8, 14, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Ahmed Raza \"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:48:08'),
(9, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:51:20'),
(10, 14, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Ahmed Raza \"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:51:49'),
(11, 14, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Ahmed Raza \"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:57:16'),
(12, 18, '{\"role\":\"5\",\"roleText\":\"Teacher\",\"name\":\"Arsalan Ali\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-02 21:58:15'),
(13, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 11:35:39'),
(14, 14, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Ahmed Raza \"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 12:02:49'),
(15, 14, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Ahmed Raza \"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 12:04:58'),
(16, 9, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Ali\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 12:05:54'),
(17, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 12:08:51'),
(18, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 12:14:08'),
(19, 18, '{\"role\":\"5\",\"roleText\":\"Teacher\",\"name\":\"Arsalan Ali\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 12:35:08'),
(20, 18, '{\"role\":\"5\",\"roleText\":\"Teacher\",\"name\":\"Arsalan Ali\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 14:54:16'),
(21, 9, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Ali\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 15:01:31'),
(22, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 16:32:03'),
(23, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 16:35:47'),
(24, 2, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Bsck Office Mgr.\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 16:58:27'),
(25, 2, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Bsck Office Mgr.\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 16:58:47'),
(26, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 17:08:41'),
(27, 2, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Bsck Office Mgr.\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 17:09:42'),
(28, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 17:09:50'),
(29, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 17:10:12'),
(30, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 17:13:52'),
(31, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 17:14:11'),
(32, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 17:18:53'),
(33, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 17:25:56'),
(34, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-03 17:26:45'),
(35, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-04 09:57:39'),
(36, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-04 12:43:41'),
(37, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 8.1', '2018-10-04 12:49:07'),
(38, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', '2018-10-16 19:55:07'),
(39, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', '2018-10-17 11:07:13'),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 69.0.3497.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"ARTT Owners\",\"name\":\"Tariq Tanoli\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Firefox 63.0', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 8.1', NULL),
(NULL, 13, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Zaki\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Rehan\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 15, '{\"role\":\"2\",\"roleText\":\"Manager\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL),
(NULL, 1, '{\"role\":\"1\",\"roleText\":\"Group Director\",\"name\":\"Shahbaz\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reset_password`
--

CREATE TABLE `tbl_reset_password` (
  `id` bigint(20) DEFAULT NULL,
  `email` varchar(384) DEFAULT NULL,
  `activation_id` varchar(96) DEFAULT NULL,
  `agent` varchar(1536) DEFAULT NULL,
  `client_ip` varchar(96) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  `createdBy` bigint(20) DEFAULT NULL,
  `createdDtm` datetime DEFAULT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_reset_password`
--

INSERT INTO `tbl_reset_password` (`id`, `email`, `activation_id`, `agent`, `client_ip`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(1, 'admin@example.com', 'hVJbqGLTdn0ryag', 'Chrome 69.0.3497.100', '::1', 0, 1, '2018-10-02 18:18:56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) DEFAULT NULL,
  `role` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
(1, 'Group Director'),
(2, 'Manager'),
(3, 'Employee'),
(6, 'Accountant'),
(7, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) DEFAULT NULL,
  `email` varchar(384) DEFAULT NULL,
  `password` varchar(384) DEFAULT NULL,
  `name` varchar(384) DEFAULT NULL,
  `mobile` varchar(60) DEFAULT NULL,
  `roleId` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDtm` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `mobile`, `roleId`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(13, 'zk@interflow.com', '$2y$10$NrmwkXQhclGMSs0l8gZIAumOnnFxwcpKhCdN5FHvsxp1YXwQjiq82', 'Zaki', '0333221171', 3, 0, 1, '2018-10-02 11:53:31', NULL, NULL),
(1, 'admin@interflow.com', '$2y$10$a/DiXlblQsColAc9PGGZCOaDB2szWITNKQnR.5VobZ3cvs.e0mQNS', 'Shahbaz', '03042232133', 1, 0, 14, '2018-10-02 18:51:07', NULL, NULL),
(NULL, 'shah@interflow.com', '123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(NULL, 'shah@interflow.com', '123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `videoinfo`
--

CREATE TABLE `videoinfo` (
  `VideoID` int(9) NOT NULL COMMENT 'Video ID',
  `AdsID` int(4) NOT NULL,
  `FileName` varchar(300) DEFAULT NULL,
  `FilePath` varchar(300) DEFAULT NULL,
  `VideoFormat` varbinary(20) DEFAULT NULL,
  `Vresolution_x` decimal(15,3) DEFAULT NULL,
  `Vresolution_y` decimal(15,3) DEFAULT NULL,
  `Vduration` int(15) DEFAULT NULL,
  `Vplaytime_seconds` int(15) DEFAULT NULL,
  `Vplaytime_string` time(6) DEFAULT NULL,
  `fourcc_lookup` int(15) DEFAULT NULL,
  `Vframe_rate` int(15) DEFAULT NULL,
  `Vframe_count` int(15) DEFAULT NULL,
  `Comments` varchar(20) DEFAULT NULL,
  `brandID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videoinfo`
--

INSERT INTO `videoinfo` (`VideoID`, `AdsID`, `FileName`, `FilePath`, `VideoFormat`, `Vresolution_x`, `Vresolution_y`, `Vduration`, `Vplaytime_seconds`, `Vplaytime_string`, `fourcc_lookup`, `Vframe_rate`, `Vframe_count`, `Comments`, `brandID`) VALUES
(1, 113, '7up (1).mp4', 'uploads\\7up/7up (1).mp4', 0x6d7034, '1280.000', '720.000', 17564734, 195, '03:15:00.000000', 0, 25, 4879, 'Undetermined', 0),
(2, 114, '7up (2).mp4', 'uploads\\7up/7up (2).mp4', 0x6d7034, '854.000', '480.000', 202548, 203, '03:23:00.000000', 0, 25, 5062, 'Undetermined', 0),
(3, 115, '7up (3).mp4', 'uploads\\7up/7up (3).mp4', 0x6d7034, '854.000', '480.000', 158012, 158, '02:38:00.000000', 0, 25, 3949, 'Undetermined', 0),
(4, 116, '7up (4).mp4', 'uploads\\7up/7up (4).mp4', 0x6d7034, '854.000', '450.000', 202061, 202, '03:22:00.000000', 0, 24, 4848, 'Undetermined', 0),
(5, 117, '7up (5).mp4', 'uploads\\7up/7up (5).mp4', 0x6d7034, '854.000', '480.000', 126108, 126, '02:06:00.000000', 0, 25, 3151, 'Undetermined', 0),
(6, 118, '7up (6).mp4', 'uploads\\7up/7up (6).mp4', 0x6d7034, '1280.000', '720.000', 19974269, 222, '03:42:00.000000', 0, 25, 5548, 'Undetermined', 0),
(7, 119, '7 Up Desert Tvc-02 30sec.mov', 'uploads\\7up/7up Desert/7 Up Desert Tvc-02 30sec.mov', 0x6d7034, '720.000', '576.000', 981, 39, '00:39:00.000000', 0, 25, 0, 'English', 0),
(8, 120, '7 Up tvc-11 (2007) redo.mov', 'uploads\\7up/7up Desert/7 Up tvc-11 (2007) redo.mov', 0x6d7034, '720.000', '576.000', 899, 36, '00:36:00.000000', 0, 25, 0, 'English', 0),
(9, 121, '7 Up tvc-11 (2007).mov', 'uploads\\7up/7up Desert/7 Up tvc-11 (2007).mov', 0x6d7034, '720.000', '576.000', 914, 37, '00:37:00.000000', 0, 25, 0, 'English', 0),
(10, 122, '7up tape-01 (Desert tvc-03) 35sec.mp4', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-03) 35sec.mp4', 0x6d7034, '720.000', '576.000', 3600000, 40, '00:40:00.000000', 0, 25, 999, 'English', 0),
(11, 123, '7up tape-01 (Desert tvc-04) 05sec.mp4', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-04) 05sec.mp4', 0x6d7034, '720.000', '576.000', 1317120, 15, '00:15:00.000000', 0, 25, 365, 'English', 0),
(12, 124, '7up tape-01 (Desert tvc-29) 28sec.mp4', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-29) 28sec.mp4', 0x6d7034, '720.000', '576.000', 3686400, 41, '00:41:00.000000', 0, 25, 1023, 'English', 0),
(13, 125, '7up tape-01 (Desert tvc-30) 28sec.mp4', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-30) 28sec.mp4', 0x6d7034, '720.000', '576.000', 3538560, 39, '00:39:00.000000', 0, 25, 982, 'English', 0),
(14, 126, '7 Up Desert Tvc-02 30sec.mov', 'uploads\\7up/7up Desert 2/7 Up Desert Tvc-02 30sec.mov', 0x6d7034, '720.000', '576.000', 981, 39, '00:39:00.000000', 0, 25, 0, 'English', 0),
(15, 127, '7 Up tvc-11 (2007) redo.mov', 'uploads\\7up/7up Desert 2/7 Up tvc-11 (2007) redo.mov', 0x6d7034, '720.000', '576.000', 899, 36, '00:36:00.000000', 0, 25, 0, 'English', 0),
(16, 128, '7 Up tvc-11 (2007).mov', 'uploads\\7up/7up Desert 2/7 Up tvc-11 (2007).mov', 0x6d7034, '720.000', '576.000', 914, 37, '00:37:00.000000', 0, 25, 0, 'English', 0),
(17, 129, '7up tape-01 (Desert tvc-03) 35sec.mp4', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-03) 35sec.mp4', 0x6d7034, '720.000', '576.000', 3600000, 40, '00:40:00.000000', 0, 25, 999, 'English', 0),
(18, 130, '7up tape-01 (Desert tvc-04) 05sec.mp4', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-04) 05sec.mp4', 0x6d7034, '720.000', '576.000', 1317120, 15, '00:15:00.000000', 0, 25, 365, 'English', 0),
(19, 131, '7up tape-01 (Desert tvc-29) 28sec.mp4', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-29) 28sec.mp4', 0x6d7034, '720.000', '576.000', 3686400, 41, '00:41:00.000000', 0, 25, 1023, 'English', 0),
(20, 132, '7up tape-01 (Desert tvc-30) 28sec.mp4', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-30) 28sec.mp4', 0x6d7034, '720.000', '576.000', 3538560, 39, '00:39:00.000000', 0, 25, 982, 'English', 0),
(21, 133, '7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(22, 134, '7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(23, 135, '7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(24, 136, '7up tape-01 (Fido tvc-07) 38sec.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(25, 137, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(26, 138, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(27, 139, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(28, 140, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(29, 141, '7up tape-01 (Fido tvc-19) 45sec.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-19) 45sec.mp4', 0x6d7034, '720.000', '576.000', 4241280, 47, '00:47:00.000000', 0, 25, 1177, 'English', 0),
(30, 142, '7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', 0x6d7034, '720.000', '576.000', 2035200, 23, '00:23:00.000000', 0, 25, 564, 'English', 0),
(31, 143, '7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 2035200, 23, '00:23:00.000000', 0, 25, 564, 'English', 0),
(32, 144, '7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', 0x6d7034, '720.000', '576.000', 1036800, 12, '00:12:00.000000', 0, 25, 287, 'English', 0),
(33, 145, '7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 1036800, 12, '00:12:00.000000', 0, 25, 287, 'English', 0),
(34, 146, '7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', 0x6d7034, '720.000', '576.000', 1036800, 12, '00:12:00.000000', 0, 25, 287, 'English', 0),
(35, 147, '7up tape-01 (Fido tvc-21) 10sec.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec.mp4', 0x6d7034, '720.000', '576.000', 1036800, 12, '00:12:00.000000', 0, 25, 287, 'English', 0),
(36, 148, '7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', 0x6d7034, '720.000', '576.000', 493440, 5, '00:05:00.000000', 0, 25, 136, 'English', 0),
(37, 149, '7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 493440, 5, '00:05:00.000000', 0, 25, 136, 'English', 0),
(38, 150, 'Abbott.mp4', 'uploads\\7up/7up Fido/Abbott.mp4', 0x6d7034, '720.000', '576.000', 1545600, 17, '00:17:00.000000', 0, 25, 428, 'English', 0),
(39, 151, 'AIDS.mp4', 'uploads\\7up/7up Fido/AIDS.mp4', 0x6d7034, '720.000', '576.000', 1545600, 17, '00:17:00.000000', 0, 25, 428, 'English', 0),
(40, 152, 'Ansar-Burney-Trust.mp4', 'uploads\\7up/7up Fido/Ansar-Burney-Trust.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(41, 153, 'Audi-Cars.mp4', 'uploads\\7up/7up Fido/Audi-Cars.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(42, 154, 'B.P.mp4', 'uploads\\7up/7up Fido/B.P.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(43, 155, 'BAIT-UL-SUKOON.mp4', 'uploads\\7up/7up Fido/BAIT-UL-SUKOON.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(44, 156, 'BARBIE.mp4', 'uploads\\7up/7up Fido/BARBIE.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(45, 157, 'Barclays.mp4', 'uploads\\7up/7up Fido/Barclays.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(46, 158, 'BATA.mp4', 'uploads\\7up/7up Fido/BATA.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(47, 159, 'Bonanza.mp4', 'uploads\\7up/7up Fido/Bonanza.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(48, 160, 'BRAUN.mp4', 'uploads\\7up/7up Fido/BRAUN.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(49, 161, 'Butterfly.mp4', 'uploads\\7up/7up Fido/Butterfly.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(50, 162, 'Capstan-Cigarette.mp4', 'uploads\\7up/7up Fido/Capstan-Cigarette.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(51, 163, 'CASTROL.mp4', 'uploads\\7up/7up Fido/CASTROL.mp4', 0x6d7034, '720.000', '576.000', 1545600, 17, '00:17:00.000000', 0, 25, 428, 'English', 0),
(52, 164, 'Cheetos.mp4', 'uploads\\7up/7up Fido/Cheetos.mp4', 0x6d7034, '720.000', '576.000', 1411200, 16, '00:16:00.000000', 0, 25, 391, 'English', 0),
(53, 165, 'CITIBANK.mp4', 'uploads\\7up/7up Fido/CITIBANK.mp4', 0x6d7034, '720.000', '576.000', 1411200, 16, '00:16:00.000000', 0, 25, 391, 'English', 0),
(54, 166, 'Citizens-Foundation.mp4', 'uploads\\7up/7up Fido/Citizens-Foundation.mp4', 0x6d7034, '720.000', '576.000', 1176960, 13, '00:13:00.000000', 0, 25, 326, 'English', 0),
(55, 167, 'Coca-Cola.mp4', 'uploads\\7up/7up Fido/Coca-Cola.mp4', 0x6d7034, '720.000', '576.000', 1411200, 16, '00:16:00.000000', 0, 25, 391, 'English', 0),
(56, 168, 'Colgate.mp4', 'uploads\\7up/7up Fido/Colgate.mp4', 0x6d7034, '720.000', '576.000', 1176960, 13, '00:13:00.000000', 0, 25, 326, 'English', 0),
(57, 169, 'Cricket.mp4', 'uploads\\7up/7up Fido/Cricket.mp4', 0x6d7034, '720.000', '576.000', 1176960, 13, '00:13:00.000000', 0, 25, 326, 'English', 0),
(58, 170, 'Dalda-Foods.mp4', 'uploads\\7up/7up Fido/Dalda-Foods.mp4', 0x6d7034, '720.000', '576.000', 1176960, 13, '00:13:00.000000', 0, 25, 326, 'English', 0),
(59, 171, 'Depilex-Smileagain-Foundation.mp4', 'uploads\\7up/7up Fido/Depilex-Smileagain-Foundation.mp4', 0x6d7034, '720.000', '576.000', 4241280, 47, '00:47:00.000000', 0, 25, 1177, 'English', 0),
(60, 172, 'DETTOL.mp4', 'uploads\\7up/7up Fido/DETTOL.mp4', 0x6d7034, '720.000', '576.000', 493440, 5, '00:05:00.000000', 0, 25, 136, 'English', 0),
(61, 173, 'Dhanak-Clinics.mp4', 'uploads\\7up/7up Fido/Dhanak-Clinics.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(62, 174, 'Dhanak-Fashion.mp4', 'uploads\\7up/7up Fido/Dhanak-Fashion.mp4', 0x6d7034, '720.000', '576.000', 493440, 5, '00:05:00.000000', 0, 25, 136, 'English', 0),
(63, 175, 'DHL.mp4', 'uploads\\7up/7up Fido/DHL.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(64, 176, 'DING-DONG-BUBBLE.mp4', 'uploads\\7up/7up Fido/DING-DONG-BUBBLE.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(65, 177, 'Drakkar-Noir.mp4', 'uploads\\7up/7up Fido/Drakkar-Noir.mp4', 0x6d7034, '720.000', '576.000', 4241280, 47, '00:47:00.000000', 0, 25, 1177, 'English', 0),
(66, 178, 'Fanta.mp4', 'uploads\\7up/7up Fido/Fanta.mp4', 0x6d7034, '720.000', '576.000', 2760960, 31, '00:31:00.000000', 0, 25, 766, 'English', 0),
(67, 179, 'Gul-Ahmed.mp4', 'uploads\\7up/7up Fido/Gul-Ahmed.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(68, 180, 'HILAL-FOODS.mp4', 'uploads\\7up/7up Fido/HILAL-FOODS.mp4', 0x6d7034, '720.000', '576.000', 2035200, 23, '00:23:00.000000', 0, 25, 564, 'English', 0),
(69, 181, 'Ismail-Industries-Limited.mp4', 'uploads\\7up/7up Fido/Ismail-Industries-Limited.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(70, 182, 'MITCHELLS.mp4', 'uploads\\7up/7up Fido/MITCHELLS.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(71, 183, 'NATIONAL-FOODS.mp4', 'uploads\\7up/7up Fido/NATIONAL-FOODS.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(72, 184, 'Nestle-Pakistan.mp4', 'uploads\\7up/7up Fido/Nestle-Pakistan.mp4', 0x6d7034, '720.000', '576.000', 1411200, 16, '00:16:00.000000', 0, 25, 391, 'English', 0),
(73, 185, 'NESTLE.mp4', 'uploads\\7up/7up Fido/NESTLE.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(74, 186, 'Pakola.mp4', 'uploads\\7up/7up Fido/Pakola.mp4', 0x6d7034, '720.000', '576.000', 2760960, 31, '00:31:00.000000', 0, 25, 766, 'English', 0),
(75, 187, 'Pepsi.mp4', 'uploads\\7up/7up Fido/Pepsi.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(76, 188, 'Philip-Morris-Pakistan-Ltd.mp4', 'uploads\\7up/7up Fido/Philip-Morris-Pakistan-Ltd.mp4', 0x6d7034, '720.000', '576.000', 2035200, 23, '00:23:00.000000', 0, 25, 564, 'English', 0),
(77, 189, 'Reckitt-Benckiser.mp4', 'uploads\\7up/7up Fido/Reckitt-Benckiser.mp4', 0x6d7034, '720.000', '576.000', 4241280, 47, '00:47:00.000000', 0, 25, 1177, 'English', 0),
(78, 190, 'Red-Bull.mp4', 'uploads\\7up/7up Fido/Red-Bull.mp4', 0x6d7034, '720.000', '576.000', 2760960, 31, '00:31:00.000000', 0, 25, 766, 'English', 0),
(79, 191, 'Sana-Safinaz.mp4', 'uploads\\7up/7up Fido/Sana-Safinaz.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(80, 192, 'SHAN-FOOD.mp4', 'uploads\\7up/7up Fido/SHAN-FOOD.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(81, 193, '7up (1).mp4', 'uploads\\7up/7up (1).mp4', 0x6d7034, '1280.000', '720.000', 17564734, 195, '03:15:00.000000', 0, 25, 4879, 'Undetermined', 0),
(82, 194, '7up (2).mp4', 'uploads\\7up/7up (2).mp4', 0x6d7034, '854.000', '480.000', 202548, 203, '03:23:00.000000', 0, 25, 5062, 'Undetermined', 0),
(83, 195, '7up (3).mp4', 'uploads\\7up/7up (3).mp4', 0x6d7034, '854.000', '480.000', 158012, 158, '02:38:00.000000', 0, 25, 3949, 'Undetermined', 0),
(84, 196, '7up (4).mp4', 'uploads\\7up/7up (4).mp4', 0x6d7034, '854.000', '450.000', 202061, 202, '03:22:00.000000', 0, 24, 4848, 'Undetermined', 0),
(85, 197, '7up (5).mp4', 'uploads\\7up/7up (5).mp4', 0x6d7034, '854.000', '480.000', 126108, 126, '02:06:00.000000', 0, 25, 3151, 'Undetermined', 0),
(86, 198, '7up (6).mp4', 'uploads\\7up/7up (6).mp4', 0x6d7034, '1280.000', '720.000', 19974269, 222, '03:42:00.000000', 0, 25, 5548, 'Undetermined', 0),
(87, 199, '7 Up Desert Tvc-02 30sec.mov', 'uploads\\7up/7up Desert/7 Up Desert Tvc-02 30sec.mov', 0x6d7034, '720.000', '576.000', 981, 39, '00:39:00.000000', 0, 25, 0, 'English', 0),
(88, 200, '7 Up tvc-11 (2007) redo.mov', 'uploads\\7up/7up Desert/7 Up tvc-11 (2007) redo.mov', 0x6d7034, '720.000', '576.000', 899, 36, '00:36:00.000000', 0, 25, 0, 'English', 0),
(89, 201, '7 Up tvc-11 (2007).mov', 'uploads\\7up/7up Desert/7 Up tvc-11 (2007).mov', 0x6d7034, '720.000', '576.000', 914, 37, '00:37:00.000000', 0, 25, 0, 'English', 0),
(90, 202, '7up tape-01 (Desert tvc-03) 35sec.mp4', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-03) 35sec.mp4', 0x6d7034, '720.000', '576.000', 3600000, 40, '00:40:00.000000', 0, 25, 999, 'English', 0),
(91, 203, '7up tape-01 (Desert tvc-04) 05sec.mp4', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-04) 05sec.mp4', 0x6d7034, '720.000', '576.000', 1317120, 15, '00:15:00.000000', 0, 25, 365, 'English', 0),
(92, 204, '7up tape-01 (Desert tvc-29) 28sec.mp4', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-29) 28sec.mp4', 0x6d7034, '720.000', '576.000', 3686400, 41, '00:41:00.000000', 0, 25, 1023, 'English', 0),
(93, 205, '7up tape-01 (Desert tvc-30) 28sec.mp4', 'uploads\\7up/7up Desert/7up tape-01 (Desert tvc-30) 28sec.mp4', 0x6d7034, '720.000', '576.000', 3538560, 39, '00:39:00.000000', 0, 25, 982, 'English', 0),
(94, 206, '7 Up Desert Tvc-02 30sec.mov', 'uploads\\7up/7up Desert 2/7 Up Desert Tvc-02 30sec.mov', 0x6d7034, '720.000', '576.000', 981, 39, '00:39:00.000000', 0, 25, 0, 'English', 0),
(95, 207, '7 Up tvc-11 (2007) redo.mov', 'uploads\\7up/7up Desert 2/7 Up tvc-11 (2007) redo.mov', 0x6d7034, '720.000', '576.000', 899, 36, '00:36:00.000000', 0, 25, 0, 'English', 0),
(96, 208, '7 Up tvc-11 (2007).mov', 'uploads\\7up/7up Desert 2/7 Up tvc-11 (2007).mov', 0x6d7034, '720.000', '576.000', 914, 37, '00:37:00.000000', 0, 25, 0, 'English', 0),
(97, 209, '7up tape-01 (Desert tvc-03) 35sec.mp4', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-03) 35sec.mp4', 0x6d7034, '720.000', '576.000', 3600000, 40, '00:40:00.000000', 0, 25, 999, 'English', 0),
(98, 210, '7up tape-01 (Desert tvc-04) 05sec.mp4', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-04) 05sec.mp4', 0x6d7034, '720.000', '576.000', 1317120, 15, '00:15:00.000000', 0, 25, 365, 'English', 0),
(99, 211, '7up tape-01 (Desert tvc-29) 28sec.mp4', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-29) 28sec.mp4', 0x6d7034, '720.000', '576.000', 3686400, 41, '00:41:00.000000', 0, 25, 1023, 'English', 0),
(100, 212, '7up tape-01 (Desert tvc-30) 28sec.mp4', 'uploads\\7up/7up Desert 2/7up tape-01 (Desert tvc-30) 28sec.mp4', 0x6d7034, '720.000', '576.000', 3538560, 39, '00:39:00.000000', 0, 25, 982, 'English', 0),
(101, 213, '7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy (4) - Copy.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(102, 214, '7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy (4).mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(103, 215, '7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec - Copy.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(104, 216, '7up tape-01 (Fido tvc-07) 38sec.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-07) 38sec.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(105, 217, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy (2) - Copy.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(106, 218, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy (2).mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(107, 219, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(108, 220, '7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-13) 45sec - Copy - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(109, 221, '7up tape-01 (Fido tvc-19) 45sec.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-19) 45sec.mp4', 0x6d7034, '720.000', '576.000', 4241280, 47, '00:47:00.000000', 0, 25, 1177, 'English', 0),
(110, 222, '7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-20) 22sec - Copy (2).mp4', 0x6d7034, '720.000', '576.000', 2035200, 23, '00:23:00.000000', 0, 25, 564, 'English', 0),
(111, 223, '7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-20) 22sec - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 2035200, 23, '00:23:00.000000', 0, 25, 564, 'English', 0),
(112, 224, '7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy (2).mp4', 0x6d7034, '720.000', '576.000', 1036800, 12, '00:12:00.000000', 0, 25, 287, 'English', 0),
(113, 225, '7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 1036800, 12, '00:12:00.000000', 0, 25, 287, 'English', 0),
(114, 226, '7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec - Copy.mp4', 0x6d7034, '720.000', '576.000', 1036800, 12, '00:12:00.000000', 0, 25, 287, 'English', 0),
(115, 227, '7up tape-01 (Fido tvc-21) 10sec.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-21) 10sec.mp4', 0x6d7034, '720.000', '576.000', 1036800, 12, '00:12:00.000000', 0, 25, 287, 'English', 0),
(116, 228, '7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-26) 05sec - Copy (2).mp4', 0x6d7034, '720.000', '576.000', 493440, 5, '00:05:00.000000', 0, 25, 136, 'English', 0),
(117, 229, '7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', 'uploads\\7up/7up Fido/7up tape-01 (Fido tvc-26) 05sec - Copy - Copy.mp4', 0x6d7034, '720.000', '576.000', 493440, 5, '00:05:00.000000', 0, 25, 136, 'English', 0),
(118, 230, 'Abbott.mp4', 'uploads\\7up/7up Fido/Abbott.mp4', 0x6d7034, '720.000', '576.000', 1545600, 17, '00:17:00.000000', 0, 25, 428, 'English', 0),
(119, 231, 'AIDS.mp4', 'uploads\\7up/7up Fido/AIDS.mp4', 0x6d7034, '720.000', '576.000', 1545600, 17, '00:17:00.000000', 0, 25, 428, 'English', 0),
(120, 232, 'Ansar-Burney-Trust.mp4', 'uploads\\7up/7up Fido/Ansar-Burney-Trust.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(121, 233, 'Audi-Cars.mp4', 'uploads\\7up/7up Fido/Audi-Cars.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(122, 234, 'B.P.mp4', 'uploads\\7up/7up Fido/B.P.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(123, 235, 'BAIT-UL-SUKOON.mp4', 'uploads\\7up/7up Fido/BAIT-UL-SUKOON.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(124, 236, 'BARBIE.mp4', 'uploads\\7up/7up Fido/BARBIE.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(125, 237, 'Barclays.mp4', 'uploads\\7up/7up Fido/Barclays.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(126, 238, 'BATA.mp4', 'uploads\\7up/7up Fido/BATA.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(127, 239, 'Bonanza.mp4', 'uploads\\7up/7up Fido/Bonanza.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(128, 240, 'BRAUN.mp4', 'uploads\\7up/7up Fido/BRAUN.mp4', 0x6d7034, '720.000', '576.000', 4801920, 53, '00:53:00.000000', 0, 25, 1333, 'English', 0),
(129, 241, 'Butterfly.mp4', 'uploads\\7up/7up Fido/Butterfly.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(130, 242, 'Capstan-Cigarette.mp4', 'uploads\\7up/7up Fido/Capstan-Cigarette.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(131, 243, 'CASTROL.mp4', 'uploads\\7up/7up Fido/CASTROL.mp4', 0x6d7034, '720.000', '576.000', 1545600, 17, '00:17:00.000000', 0, 25, 428, 'English', 0),
(132, 244, 'Cheetos.mp4', 'uploads\\7up/7up Fido/Cheetos.mp4', 0x6d7034, '720.000', '576.000', 1411200, 16, '00:16:00.000000', 0, 25, 391, 'English', 0),
(133, 245, 'CITIBANK.mp4', 'uploads\\7up/7up Fido/CITIBANK.mp4', 0x6d7034, '720.000', '576.000', 1411200, 16, '00:16:00.000000', 0, 25, 391, 'English', 0),
(134, 246, 'Citizens-Foundation.mp4', 'uploads\\7up/7up Fido/Citizens-Foundation.mp4', 0x6d7034, '720.000', '576.000', 1176960, 13, '00:13:00.000000', 0, 25, 326, 'English', 0),
(135, 247, 'Coca-Cola.mp4', 'uploads\\7up/7up Fido/Coca-Cola.mp4', 0x6d7034, '720.000', '576.000', 1411200, 16, '00:16:00.000000', 0, 25, 391, 'English', 0),
(136, 248, 'Colgate.mp4', 'uploads\\7up/7up Fido/Colgate.mp4', 0x6d7034, '720.000', '576.000', 1176960, 13, '00:13:00.000000', 0, 25, 326, 'English', 0),
(137, 249, 'Cricket.mp4', 'uploads\\7up/7up Fido/Cricket.mp4', 0x6d7034, '720.000', '576.000', 1176960, 13, '00:13:00.000000', 0, 25, 326, 'English', 0),
(138, 250, 'Dalda-Foods.mp4', 'uploads\\7up/7up Fido/Dalda-Foods.mp4', 0x6d7034, '720.000', '576.000', 1176960, 13, '00:13:00.000000', 0, 25, 326, 'English', 0),
(139, 251, 'Depilex-Smileagain-Foundation.mp4', 'uploads\\7up/7up Fido/Depilex-Smileagain-Foundation.mp4', 0x6d7034, '720.000', '576.000', 4241280, 47, '00:47:00.000000', 0, 25, 1177, 'English', 0),
(140, 252, 'DETTOL.mp4', 'uploads\\7up/7up Fido/DETTOL.mp4', 0x6d7034, '720.000', '576.000', 493440, 5, '00:05:00.000000', 0, 25, 136, 'English', 0),
(141, 253, 'Dhanak-Clinics.mp4', 'uploads\\7up/7up Fido/Dhanak-Clinics.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(142, 254, 'Dhanak-Fashion.mp4', 'uploads\\7up/7up Fido/Dhanak-Fashion.mp4', 0x6d7034, '720.000', '576.000', 493440, 5, '00:05:00.000000', 0, 25, 136, 'English', 0),
(143, 255, 'DHL.mp4', 'uploads\\7up/7up Fido/DHL.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(144, 256, 'DING-DONG-BUBBLE.mp4', 'uploads\\7up/7up Fido/DING-DONG-BUBBLE.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(145, 257, 'Drakkar-Noir.mp4', 'uploads\\7up/7up Fido/Drakkar-Noir.mp4', 0x6d7034, '720.000', '576.000', 4241280, 47, '00:47:00.000000', 0, 25, 1177, 'English', 0),
(146, 258, 'Fanta.mp4', 'uploads\\7up/7up Fido/Fanta.mp4', 0x6d7034, '720.000', '576.000', 2760960, 31, '00:31:00.000000', 0, 25, 766, 'English', 0),
(147, 259, 'Gul-Ahmed.mp4', 'uploads\\7up/7up Fido/Gul-Ahmed.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(148, 260, 'HILAL-FOODS.mp4', 'uploads\\7up/7up Fido/HILAL-FOODS.mp4', 0x6d7034, '720.000', '576.000', 2035200, 23, '00:23:00.000000', 0, 25, 564, 'English', 0),
(149, 261, 'Ismail-Industries-Limited.mp4', 'uploads\\7up/7up Fido/Ismail-Industries-Limited.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(150, 262, 'MITCHELLS.mp4', 'uploads\\7up/7up Fido/MITCHELLS.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(151, 263, 'NATIONAL-FOODS.mp4', 'uploads\\7up/7up Fido/NATIONAL-FOODS.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(152, 264, 'Nestle-Pakistan.mp4', 'uploads\\7up/7up Fido/Nestle-Pakistan.mp4', 0x6d7034, '720.000', '576.000', 1411200, 16, '00:16:00.000000', 0, 25, 391, 'English', 0),
(153, 265, 'NESTLE.mp4', 'uploads\\7up/7up Fido/NESTLE.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(154, 266, 'Pakola.mp4', 'uploads\\7up/7up Fido/Pakola.mp4', 0x6d7034, '720.000', '576.000', 2760960, 31, '00:31:00.000000', 0, 25, 766, 'English', 0),
(155, 267, 'Pepsi.mp4', 'uploads\\7up/7up Fido/Pepsi.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(156, 268, 'Philip-Morris-Pakistan-Ltd.mp4', 'uploads\\7up/7up Fido/Philip-Morris-Pakistan-Ltd.mp4', 0x6d7034, '720.000', '576.000', 2035200, 23, '00:23:00.000000', 0, 25, 564, 'English', 0),
(157, 269, 'Reckitt-Benckiser.mp4', 'uploads\\7up/7up Fido/Reckitt-Benckiser.mp4', 0x6d7034, '720.000', '576.000', 4241280, 47, '00:47:00.000000', 0, 25, 1177, 'English', 0),
(158, 270, 'Red-Bull.mp4', 'uploads\\7up/7up Fido/Red-Bull.mp4', 0x6d7034, '720.000', '576.000', 2760960, 31, '00:31:00.000000', 0, 25, 766, 'English', 0),
(159, 271, 'Sana-Safinaz.mp4', 'uploads\\7up/7up Fido/Sana-Safinaz.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(160, 272, 'SHAN-FOOD.mp4', 'uploads\\7up/7up Fido/SHAN-FOOD.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(161, 273, 'SHEZAN.mp4', 'uploads\\7up/7up Fido/SHEZAN.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(162, 274, 'Sprite.mp4', 'uploads\\7up/7up Fido/Sprite.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(163, 275, 'Sting.mp4', 'uploads\\7up/7up Fido/Sting.mp4', 0x6d7034, '720.000', '576.000', 5030400, 56, '00:56:00.000000', 0, 25, 1396, 'English', 0),
(164, 276, 'UFONE.mp4', 'uploads\\7up/7up Fido/UFONE.mp4', 0x6d7034, '720.000', '576.000', 2760960, 31, '00:31:00.000000', 0, 25, 766, 'English', 0),
(165, 277, 'Unilever.mp4', 'uploads\\7up/7up Fido/Unilever.mp4', 0x6d7034, '720.000', '576.000', 3546240, 39, '00:39:00.000000', 0, 25, 984, 'English', 0),
(166, 278, 'ZONG.mp4', 'uploads\\7up/7up Fido/ZONG.mp4', 0x6d7034, '720.000', '576.000', 1545600, 17, '00:17:00.000000', 0, 25, 428, 'English', 0),
(167, 279, '7UP Fido Prom-01 35sec.mp4', 'uploads\\7up/7up Fido Promo/7UP Fido Prom-01 35sec.mp4', 0x6d7034, '720.000', '576.000', 4608000, 51, '00:51:00.000000', 0, 25, 1279, 'English', 0),
(168, 280, '7UP Fido Prom-01 36sec.mp4', 'uploads\\7up/7up Fido Promo/7UP Fido Prom-01 36sec.mp4', 0x6d7034, '720.000', '576.000', 4266240, 47, '00:47:00.000000', 0, 25, 1184, 'English', 0),
(169, 281, '7up.mov', 'uploads\\7up/7up.mov', 0x6d7034, '1280.000', '720.000', 221983, 222, '03:42:00.000000', 0, 25, 5548, 'English', 0),
(170, 282, 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar.mp4', 'uploads\\7up/Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar.mp4', 0x6d7034, '1280.000', '676.000', 21575053, 240, '04:00:00.000000', 0, 25, 5993, 'Undetermined', 0),
(171, 283, 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar1.mp4', 'uploads\\7up/Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar1.mp4', 0x6d7034, '1280.000', '676.000', 21575053, 240, '04:00:00.000000', 0, 25, 5993, 'Undetermined', 0),
(172, 284, 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar2.mp4', 'uploads\\7up/Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar2.mp4', 0x6d7034, '1280.000', '676.000', 21575053, 240, '04:00:00.000000', 0, 25, 5993, 'Undetermined', 0),
(173, 285, 'Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar3.mp4', 'uploads\\7up/Mera_Highway_Star_Video_Song_¦_Tulsi_Kumar_Khushali_Kumar_¦_Raftaar3.mp4', 0x6d7034, '1280.000', '676.000', 21575053, 240, '04:00:00.000000', 0, 25, 5993, 'Undetermined', 0),
(174, 286, 'PANIYON_SA_Full_Song_¦_Satyameva_Jayate_¦_John_Abraham_¦_Aisha_Sharma_¦_Tulsi_Kumar_¦_Atif_Aslam.mp4', 'uploads\\7up/PANIYON_SA_Full_Song_¦_Satyameva_Jayate_¦_John_Abraham_¦_Aisha_Sharma_¦_Tulsi_Kumar_¦_Atif_Aslam.mp4', 0x6d7034, '640.000', '360.000', 206425, 206, '03:26:00.000000', 0, 24, 4953, 'Undetermined', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adscategory`
--
ALTER TABLE `adscategory`
  ADD PRIMARY KEY (`CatID`,`CatType`);

--
-- Indexes for table `adsmasterinfo`
--
ALTER TABLE `adsmasterinfo`
  ADD PRIMARY KEY (`AdsID`),
  ADD KEY `AdCatID_FK` (`CatID`),
  ADD KEY `AgencyID_FK` (`AgencyID`),
  ADD KEY `ArtistID_FK` (`ArtistID`),
  ADD KEY `ProductID_FK` (`ProductID`);

--
-- Indexes for table `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`AgencyID`,`AgencyName`);

--
-- Indexes for table `artistprofile`
--
ALTER TABLE `artistprofile`
  ADD PRIMARY KEY (`ArtistID`,`Name`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`BrandID`,`BrandName`),
  ADD KEY `SecFK` (`SectorID`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ProductID`,`ProductName`),
  ADD KEY `BrandFK` (`BrandID`);

--
-- Indexes for table `roll`
--
ALTER TABLE `roll`
  ADD PRIMARY KEY (`roll_id`);

--
-- Indexes for table `sector`
--
ALTER TABLE `sector`
  ADD PRIMARY KEY (`SectorID`,`SectorName`);

--
-- Indexes for table `signup`
--
ALTER TABLE `signup`
  ADD PRIMARY KEY (`signup_id`);

--
-- Indexes for table `videoinfo`
--
ALTER TABLE `videoinfo`
  ADD PRIMARY KEY (`VideoID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adscategory`
--
ALTER TABLE `adscategory`
  MODIFY `CatID` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Category ID Unique', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `adsmasterinfo`
--
ALTER TABLE `adsmasterinfo`
  MODIFY `AdsID` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Ads ID Unique', AUTO_INCREMENT=287;

--
-- AUTO_INCREMENT for table `agency`
--
ALTER TABLE `agency`
  MODIFY `AgencyID` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Agency ID', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `artistprofile`
--
ALTER TABLE `artistprofile`
  MODIFY `ArtistID` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Artist ID Unique', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `BrandID` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Brand ID', AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `ProductID` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Product ID', AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `roll`
--
ALTER TABLE `roll`
  MODIFY `roll_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sector`
--
ALTER TABLE `sector`
  MODIFY `SectorID` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Sector ID', AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `signup`
--
ALTER TABLE `signup`
  MODIFY `signup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=436;

--
-- AUTO_INCREMENT for table `videoinfo`
--
ALTER TABLE `videoinfo`
  MODIFY `VideoID` int(9) NOT NULL AUTO_INCREMENT COMMENT 'Video ID', AUTO_INCREMENT=175;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adsmasterinfo`
--
ALTER TABLE `adsmasterinfo`
  ADD CONSTRAINT `AdCatID_FK` FOREIGN KEY (`CatID`) REFERENCES `adscategory` (`CatID`),
  ADD CONSTRAINT `AgencyID_FK` FOREIGN KEY (`AgencyID`) REFERENCES `agency` (`AgencyID`),
  ADD CONSTRAINT `ArtistID_FK` FOREIGN KEY (`ArtistID`) REFERENCES `artistprofile` (`ArtistID`),
  ADD CONSTRAINT `ProductID_FK` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

--
-- Constraints for table `brand`
--
ALTER TABLE `brand`
  ADD CONSTRAINT `SecFK` FOREIGN KEY (`SectorID`) REFERENCES `sector` (`SectorID`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `BrandFK` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
